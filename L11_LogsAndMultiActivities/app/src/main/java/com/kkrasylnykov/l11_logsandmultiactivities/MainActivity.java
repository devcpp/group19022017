package com.kkrasylnykov.l11_logsandmultiactivities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int REQUEST_CODE_SECOND_ACTIVITY = 101;

    private EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        View btnMoveToSecondActivity = findViewById(R.id.butttonMoveToSecondAdctivityMainActivity);
        btnMoveToSecondActivity.setOnClickListener(this);

        editText = (EditText) findViewById(R.id.editTextNameMainActivity);

        Log.d("devcpp", "MainActivity -> onCreate -> " + R.layout.activity_main);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("devcpp", "MainActivity -> onStart -> ");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("devcpp", "MainActivity -> onResume -> ");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("devcpp", "MainActivity -> onRestart -> ");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("devcpp", "MainActivity -> onPause -> ");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("devcpp", "MainActivity -> onStop -> ");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("devcpp", "MainActivity -> onDestroy -> ");
    }

    @Override
    public void onClick(View view) {
        Log.d("devcpp", "MainActivity -> onClick -> " + view.getId());
        switch (view.getId()){
            case R.id.butttonMoveToSecondAdctivityMainActivity:
                String strName = editText.getText().toString();
                if ("".equals(strName)){
                    Toast.makeText(this, "Name is empty!!!!", Toast.LENGTH_LONG).show();
                    return;
                }
                Intent intent = new Intent(this,SecondActivity.class);
                intent.putExtra(SecondActivity.KEY_NAME,strName);
                startActivityForResult(intent, REQUEST_CODE_SECOND_ACTIVITY);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==REQUEST_CODE_SECOND_ACTIVITY){
            if (resultCode==RESULT_OK && data!=null){
                Bundle bundle = data.getExtras();
                if (bundle!=null){
                    editText.setText("");
                    editText.setHint(bundle.getString(SecondActivity.RESULT_KEY_DATA, "--EMPTY--"));
                }

            }
        }
    }
}
