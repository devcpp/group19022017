package com.kkrasylnykov.l11_logsandmultiactivities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity implements View.OnClickListener {

    public final static String KEY_NAME = "KEY_NAME";

    public static final int RESULT_NO = 3;

    private EditText editText;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_second);

        TextView textView = (TextView) findViewById(R.id.textViewNameSecondActivity);

        String strName = "";

        Intent intent = getIntent();
        if (intent!=null){
            /*String strName = intent.getStringExtra("key_name");
            Log.d("devcpp", "SecondActivity -> onCreate -> " + strName);*/
            Bundle bundle = intent.getExtras();
            if (bundle!=null){
                strName = bundle.getString(KEY_NAME, "--EMPTY--");
            }
        }

        textView.setText("Привет, " + strName + "!");

        View bntYes = findViewById(R.id.buttonYesSecondActivity);
        View bntNo = findViewById(R.id.buttonNoSecondActivity);

        bntYes.setOnClickListener(this);
        bntNo.setOnClickListener(this);

        editText = (EditText) findViewById(R.id.editTextSecondActivity);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("devcpp", "SecondActivity -> onStart -> ");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("devcpp", "SecondActivity -> onResume -> ");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("devcpp", "SecondActivity -> onRestart -> ");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("devcpp", "SecondActivity -> onPause -> ");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("devcpp", "SecondActivity -> onStop -> ");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("devcpp", "SecondActivity -> onDestroy -> ");
    }

    public static final String RESULT_KEY_DATA = "RESULT_KEY_DATA";

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.buttonYesSecondActivity:
                String strText = editText.getText().toString();
                Intent intent = new Intent();
                intent.putExtra(RESULT_KEY_DATA+"1",strText);
                setResult(RESULT_OK, intent);
                break;
            case R.id.buttonNoSecondActivity:
                setResult(RESULT_NO);
                break;
        }
        //onBackPressed();
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
