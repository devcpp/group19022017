package com.kkrasylnykov.l12_fragmentsexamles.activities;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.kkrasylnykov.l12_fragmentsexamles.R;
import com.kkrasylnykov.l12_fragmentsexamles.fragments.FirstFragment;
import com.kkrasylnykov.l12_fragmentsexamles.fragments.SecondFragment;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    FirstFragment firstFragment;
    SecondFragment secondFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        firstFragment = new FirstFragment();
        secondFragment = new SecondFragment();

        findViewById(R.id.btnAddFirstMainActivity).setOnClickListener(this);
        findViewById(R.id.btnRemoveFirstMainActivity).setOnClickListener(this);
        findViewById(R.id.btnAddSecondMainActivity).setOnClickListener(this);
        findViewById(R.id.btnRemoveSecondMainActivity).setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        switch (view.getId()){
            case R.id.btnAddFirstMainActivity:
                Log.d("devcpp","firstFragment.isAdded() -> " + firstFragment.isAdded());
                if (firstFragment.isAdded()){
                    transaction.replace(R.id.frameLayout1, firstFragment);
                } else {
                    transaction.add(R.id.frameLayout1, firstFragment);
                }

                break;
            case R.id.btnRemoveFirstMainActivity:
                transaction.remove(firstFragment);
                break;

            case R.id.btnAddSecondMainActivity:
                Log.d("devcpp","secondFragment.isAdded() -> " + secondFragment.isAdded());
                if (secondFragment.isAdded()){
                    transaction.replace(R.id.frameLayout2, secondFragment);
                } else {
                    transaction.add(R.id.frameLayout2, secondFragment);
                }

                break;
            case R.id.btnRemoveSecondMainActivity:
                transaction.remove(secondFragment);
                break;

        }
        transaction.commit();
    }

    public void sendData(String data){
        secondFragment.setData(data);
    }
}
