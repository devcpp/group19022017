package com.kkrasylnykov.l12_fragmentsexamles.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.kkrasylnykov.l12_fragmentsexamles.R;
import com.kkrasylnykov.l12_fragmentsexamles.activities.MainActivity;

public class FirstFragment extends Fragment implements View.OnClickListener {

    private EditText editText;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_first, container, false);
        editText = (EditText) view.findViewById(R.id.editTextFirstFragment);
        view.findViewById(R.id.btnSendData).setOnClickListener(this);

        View btn = view.findViewById(R.id.btnSendData);
        btn.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnSendData:
                String strData = editText.getText().toString();
                if ("".equals(strData)){
                    if (isAdded()){
                        Toast.makeText(getActivity(), "strData is empty", Toast.LENGTH_SHORT).show();
                    }
                    return;
                }
                Activity activity = getActivity();
                if (activity instanceof MainActivity){
                    MainActivity mainActivity = (MainActivity) activity;
                    mainActivity.sendData(strData);
                }
                break;
        }
    }
}
