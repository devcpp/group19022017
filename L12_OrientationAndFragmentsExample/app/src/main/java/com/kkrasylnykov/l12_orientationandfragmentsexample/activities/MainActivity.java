package com.kkrasylnykov.l12_orientationandfragmentsexample.activities;

import android.app.FragmentTransaction;
import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.kkrasylnykov.l12_orientationandfragmentsexample.R;
import com.kkrasylnykov.l12_orientationandfragmentsexample.fragments.ListFragment;
import com.kkrasylnykov.l12_orientationandfragmentsexample.fragments.ViewerFragment;

public class MainActivity extends AppCompatActivity {

    ListFragment listFragment;
    ViewerFragment viewerFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listFragment = new ListFragment();
        viewerFragment = new ViewerFragment();

        FragmentTransaction transaction = getFragmentManager().beginTransaction();

        if (getResources().getConfiguration().orientation== Configuration.ORIENTATION_PORTRAIT){
            transaction.add(R.id.frameLayout1, listFragment);
        } else if (getResources().getConfiguration().orientation== Configuration.ORIENTATION_LANDSCAPE) {
            transaction.add(R.id.frameLayout1, listFragment);
            transaction.add(R.id.frameLayout2, viewerFragment);
        }
        transaction.commit();

    }

    public void setButtonText(String strData){
        viewerFragment.setData(strData);
        if (getResources().getConfiguration().orientation== Configuration.ORIENTATION_PORTRAIT){
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.add(R.id.frameLayout1, viewerFragment);
            transaction.addToBackStack("");
            transaction.commit();
        }

    }
}
