package com.kkrasylnykov.l12_orientationandfragmentsexample.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.kkrasylnykov.l12_orientationandfragmentsexample.R;
import com.kkrasylnykov.l12_orientationandfragmentsexample.activities.MainActivity;

public class ListFragment extends Fragment implements View.OnClickListener {

    private final static int BUTTON_COUNT = 15;

    LinearLayout linearLayout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_list, container, false);
        linearLayout = (LinearLayout) rootView.findViewById(R.id.conteynerLinearLayoutListFragment);
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        linearLayout.removeAllViews();
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        for (int i=0; i<BUTTON_COUNT; i++){
            Button button = new Button(getActivity());
            button.setLayoutParams(layoutParams);
            button.setText("Button - " + i);
            button.setOnClickListener(this);
            linearLayout.addView(button);
        }
    }

    @Override
    public void onClick(View view) {
        if (view instanceof  Button){
            Button button = (Button) view;
            String strBtnText = button.getText().toString();
            if(isAdded()){
                Activity activity = getActivity();
                if (activity!=null && activity instanceof MainActivity){
                    MainActivity mainActivity = (MainActivity) activity;
                    mainActivity.setButtonText(strBtnText);
                }
            }
        }
    }
}
