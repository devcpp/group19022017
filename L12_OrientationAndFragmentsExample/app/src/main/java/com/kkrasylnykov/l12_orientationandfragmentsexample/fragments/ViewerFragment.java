package com.kkrasylnykov.l12_orientationandfragmentsexample.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kkrasylnykov.l12_orientationandfragmentsexample.R;

public class ViewerFragment extends Fragment {

    private TextView textView;
    private String strData;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_viewer, container, false);
        textView = (TextView) view.findViewById(R.id.textViewViewerFragment);
        if (strData!=null){
            textView.setText(strData);
        }
        return view;
    }

    public void setData(String strData){
        this.strData = strData;
        if (isAdded() && textView!=null){
            textView.setText(strData);
        }
    }
}
