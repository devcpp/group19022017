package com.kkrasylnykov.l13_customview.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.kkrasylnykov.l13_customview.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
