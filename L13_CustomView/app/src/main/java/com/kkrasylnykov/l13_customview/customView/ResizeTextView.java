package com.kkrasylnykov.l13_customview.customView;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.widget.TextView;

import com.kkrasylnykov.l13_customview.R;

public class ResizeTextView extends TextView {

    private float m_fTextMinSize = 0f;
    private float m_fTextDefSize = 0f;
    private boolean m_bIsResize = true;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public ResizeTextView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        processingAttrs(context, attrs);
    }

    public ResizeTextView(Context context) {
        super(context);
    }

    public ResizeTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        processingAttrs(context, attrs);
    }

    public ResizeTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        processingAttrs(context, attrs);
    }

    private void processingAttrs(Context context, AttributeSet attrs){
        if (attrs!=null){
            TypedArray typedArray = context.getTheme().obtainStyledAttributes(
                    attrs,
                    R.styleable.ResizeTextView,
                    0, 0);

            try {
                m_bIsResize = typedArray.getBoolean(R.styleable.ResizeTextView_isResize, true);
                m_fTextMinSize = typedArray.getDimensionPixelSize(R.styleable.ResizeTextView_minTextSize, 0);
                m_fTextDefSize = getTextSize();
            } finally {
                typedArray.recycle();
            }

        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if(m_bIsResize){
            int nMaxWidth = MeasureSpec.getSize(widthMeasureSpec);

            int nUnSpecWidth = MeasureSpec.makeMeasureSpec(0,MeasureSpec.UNSPECIFIED);
            super.onMeasure(nUnSpecWidth, heightMeasureSpec);

            int nCurrentWidth = this.getMeasuredWidth();

            Log.d("devcpp","nMaxWidth -> " + nMaxWidth);
            Log.d("devcpp","nCurrentWidth -> " + nCurrentWidth);
            if (nMaxWidth<nCurrentWidth){
                float fDelta = ((float)nMaxWidth)/nCurrentWidth;
                Log.d("devcpp","fDelta -> " + fDelta);
                float fCurrentTextSize = this.getTextSize() * fDelta;
                fCurrentTextSize = fCurrentTextSize<m_fTextMinSize?m_fTextMinSize:fCurrentTextSize;
                this.setTextSize(TypedValue.COMPLEX_UNIT_PX,fCurrentTextSize);
            }
            this.setMaxLines(1);
        }
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
}
