package com.kkrasylnykov.l13_customview.customView;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kkrasylnykov.l13_customview.R;

public class TextAndImageView extends RelativeLayout {

    private float m_fMaxTextSize;
    private float m_fMinTextSize;
    private int m_nImageResourseId;

    private TextView m_textView;
    private ImageView m_ImageView;

    public TextAndImageView(Context context) {
        super(context);
        initView(context);
    }

    public TextAndImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context, attrs);
    }

    public TextAndImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context, attrs);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public TextAndImageView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initView(context, attrs);
    }

    private void initView(Context context){
        initView(context, null);
    }

    private void initView(Context context, AttributeSet attrs){
        LayoutInflater.from(context).inflate(R.layout.custom_view_text_and_image_view,this,true);
        processingAttrs(context, attrs);
        m_textView = (TextView) findViewById(R.id.textViewTextAndImageView);
        m_ImageView = (ImageView) findViewById(R.id.imageViewTextAndImageView);
    }

    private void processingAttrs(Context context, AttributeSet attrs){
        if (attrs!=null){
            //TODO processingAttrs
            /*TypedArray typedArray = context.getTheme().obtainStyledAttributes(
                    attrs,
                    R.styleable.ResizeTextView,
                    0, 0);

            try {
                m_bIsResize = typedArray.getBoolean(R.styleable.ResizeTextView_isResize, true);
                m_fTextMinSize = typedArray.getDimensionPixelSize(R.styleable.ResizeTextView_minTextSize, 0);
                m_fTextDefSize = getTextSize();
            } finally {
                typedArray.recycle();
            }*/
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int nTextLeftMargin = m_ImageView.getMeasuredWidth() + 5;
        int nMaxHeightText = m_ImageView.getMeasuredHeight();
        int nWidthText = MeasureSpec.getSize(widthMeasureSpec) - nTextLeftMargin;

        RelativeLayout.LayoutParams params = new LayoutParams(nWidthText, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(nTextLeftMargin,0,0,0);
        m_textView.setLayoutParams(params);

        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int nCurrentHeightTextVie = m_textView.getMeasuredHeight();
        while (nCurrentHeightTextVie>nMaxHeightText){
            float fNewTextSize = m_textView.getTextSize()-5;
            m_textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, fNewTextSize);
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
            nCurrentHeightTextVie = m_textView.getMeasuredHeight();
        }




    }
}
