package com.kkrasylnykov.l14_datasaveexample.activities;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kkrasylnykov.l14_datasaveexample.R;
import com.kkrasylnykov.l14_datasaveexample.db.DbHelper;
import com.kkrasylnykov.l14_datasaveexample.toolsAndConstants.AppSettings;
import com.kkrasylnykov.l14_datasaveexample.toolsAndConstants.DbConstants;

public class MainActivity extends AppCompatActivity {

    private static final int REQUEST_CODE_TERMS_ACTIVITY = 101;

    private LinearLayout m_conteynerLinearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AppSettings appSettings = new AppSettings(this);
        if (appSettings.isShowTerms()){
            Intent intent = new Intent(this, TermsActivity.class);
            startActivityForResult(intent, REQUEST_CODE_TERMS_ACTIVITY);

            DbHelper dbHelper = new DbHelper(this);
            SQLiteDatabase db = dbHelper.getWritableDatabase();

            ContentValues values;
            try {
                for (int i=0; i<100; i++){
                    values = new ContentValues();
                    values.put(DbConstants.USER_INFO_FIELD_NAME, "Kos " + i);
                    values.put(DbConstants.USER_INFO_FIELD_SNAME, "Kras " + i);
                    values.put(DbConstants.USER_INFO_FIELD_PHONE, "095" + i);
                    values.put(DbConstants.USER_INFO_FIELD_EMAIL, "kos_" + i +"@mozgov.net");
                    values.put(DbConstants.USER_INFO_FIELD_BDAY, 876 + i * 10);

                    db.insert(DbConstants.TABLE_NAME, null, values);
                }
            } finally {
                db.close();
            }
        }

        m_conteynerLinearLayout = (LinearLayout) findViewById(R.id.contayner);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_TERMS_ACTIVITY && resultCode!=RESULT_OK){
            finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateInfo();
    }

    private void updateInfo(){
        m_conteynerLinearLayout.removeAllViews();
        DbHelper dbHelper = new DbHelper(this);
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.query(DbConstants.TABLE_NAME,null,null,null,null,null,null);

        try{
            if (cursor!=null && cursor.moveToFirst()){
                do{
                    long id = cursor.getLong(cursor.getColumnIndex(DbConstants.USER_INFO_FIELD_ID));
                    String name = cursor.getString(cursor.getColumnIndex(DbConstants.USER_INFO_FIELD_NAME));
                    String sname = cursor.getString(cursor.getColumnIndex(DbConstants.USER_INFO_FIELD_SNAME));
                    String phone = cursor.getString(cursor.getColumnIndex(DbConstants.USER_INFO_FIELD_PHONE));
                    String email = cursor.getString(cursor.getColumnIndex(DbConstants.USER_INFO_FIELD_EMAIL));
                    long bDay = cursor.getLong(cursor.getColumnIndex(DbConstants.USER_INFO_FIELD_BDAY));

                    TextView textView = new TextView(this);

                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    layoutParams.setMargins(0,5,0,5);
                    textView.setLayoutParams(layoutParams);
                    textView.setText("id -> " + id + "\t name -> " + name + "\n sname -> " + sname + "\t phone -> " + phone + "\n email -> " + email + "\t bDay -> " + bDay);
                    m_conteynerLinearLayout.addView(textView);

                    Log.d("devcpp","id -> " + id);
                    Log.d("devcpp","name -> " + name);
                    Log.d("devcpp","sname -> " + sname);
                    Log.d("devcpp","phone -> " + phone);
                    Log.d("devcpp","email -> " + email);
                    Log.d("devcpp","bDay -> " + bDay);
                } while (cursor.moveToNext());

            }
        } finally {
            if (cursor!=null){
                cursor.close();
            }
            db.close();
        }
    }
}
