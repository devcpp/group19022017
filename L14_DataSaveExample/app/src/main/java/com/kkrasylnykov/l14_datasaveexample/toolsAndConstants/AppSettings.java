package com.kkrasylnykov.l14_datasaveexample.toolsAndConstants;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class AppSettings {
    private static final String KEY_BOOLEAN_IS_SHOW_TERMS = "KEY_BOOLEAN_IS_SHOW_TERMS";

    private SharedPreferences m_SharedPreferences;

    public AppSettings(Context context){
        m_SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public boolean isShowTerms(){
        return m_SharedPreferences.getBoolean(KEY_BOOLEAN_IS_SHOW_TERMS, true);
    }

    public void setShowTerms(boolean bIsShow){
        SharedPreferences.Editor editor = m_SharedPreferences.edit();
        editor.putBoolean(KEY_BOOLEAN_IS_SHOW_TERMS, bIsShow);
        editor.commit();
    }

}
