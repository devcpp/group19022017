package com.kkrasylnykov.l14_datasaveexample.toolsAndConstants;

public class DbConstants {
    public static final String DB_NAME = "noute_db";
    public static final int DB_VERSION = 1;

    public static final String TABLE_NAME = "UserInfo";

    public static final String USER_INFO_FIELD_ID = "_id";
    public static final String USER_INFO_FIELD_NAME = "_name";
    public static final String USER_INFO_FIELD_SNAME = "_sname";
    public static final String USER_INFO_FIELD_PHONE = "_phone";
    public static final String USER_INFO_FIELD_EMAIL = "_email";
    public static final String USER_INFO_FIELD_BDAY = "_bDay";

}
