package com.kkrasylnykov.l14_datasaveexample.activities;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kkrasylnykov.l14_datasaveexample.R;
import com.kkrasylnykov.l14_datasaveexample.db.DbHelper;
import com.kkrasylnykov.l14_datasaveexample.model.UserInfo;
import com.kkrasylnykov.l14_datasaveexample.model.engines.UserInfoEngine;
import com.kkrasylnykov.l14_datasaveexample.toolsAndConstants.AppSettings;
import com.kkrasylnykov.l14_datasaveexample.toolsAndConstants.DbConstants;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int REQUEST_CODE_TERMS_ACTIVITY = 101;

    private LinearLayout m_conteynerLinearLayout;
    private ArrayList<UserInfo> arrUsers = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AppSettings appSettings = new AppSettings(this);
        if (appSettings.isShowTerms()){
            Intent intent = new Intent(this, TermsActivity.class);
            startActivityForResult(intent, REQUEST_CODE_TERMS_ACTIVITY);

            DbHelper dbHelper = new DbHelper(this);
            SQLiteDatabase db = dbHelper.getWritableDatabase();

            try {
                for (int i=0; i<5000; i++){
                    UserInfo userInfo = new UserInfo("Kos"+ i,"Kras"+ i,"095" + i,
                            "test"+ i+"@g.m",56+i*5);

                    db.insert(DbConstants.TABLE_NAME, null, userInfo.getContentValues());
                }
            } finally {
                db.close();
            }
        }

        m_conteynerLinearLayout = (LinearLayout) findViewById(R.id.contayner);
        findViewById(R.id.buttonAddUserMainActivity).setOnClickListener(this);
        findViewById(R.id.buttonRemoveAllUserMainActivity).setOnClickListener(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_TERMS_ACTIVITY && resultCode!=RESULT_OK){
            finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateInfo();
    }

    private void updateInfo(){
        m_conteynerLinearLayout.removeAllViews();
        arrUsers.clear();
        UserInfoEngine userInfoEngine = new UserInfoEngine(this);
        arrUsers.addAll(userInfoEngine.getAllUsers());
        for (UserInfo userInfo: arrUsers){
            TextView textView = new TextView(this);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins(0,5,0,5);
            textView.setLayoutParams(layoutParams);
            textView.setText("id -> " + userInfo.getId() + "\t name -> "
                    + userInfo.getName() + "\n sname -> " + userInfo.getSName()
                    + "\t phone -> " + userInfo.getPhone() + "\n email -> "
                    + userInfo.getEmail() + "\t bDay -> " + userInfo.getBDay());
            textView.setOnClickListener(this);
            textView.setTag(userInfo.getId());
            m_conteynerLinearLayout.addView(textView);
        }
    }

    @Override
    public void onClick(View view) {
        if (view.getTag()!=null && view.getTag() instanceof Long && view instanceof TextView){
            long nId = (long) view.getTag();
            Intent intent = new Intent(this, EditActivity.class);
            intent.putExtra(EditActivity.KEY_ID, nId);
            startActivity(intent);
        } else {
            switch (view.getId()){
                case R.id.buttonAddUserMainActivity:
                    Intent intent = new Intent(this, EditActivity.class);
                    startActivity(intent);
                    break;
                case R.id.buttonRemoveAllUserMainActivity:
                    UserInfoEngine userInfoEngine = new UserInfoEngine(this);
                    userInfoEngine.removeAll();
                    break;
            }
        }

    }
}
