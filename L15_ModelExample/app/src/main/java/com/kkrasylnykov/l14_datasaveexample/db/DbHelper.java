package com.kkrasylnykov.l14_datasaveexample.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.kkrasylnykov.l14_datasaveexample.toolsAndConstants.DbConstants;

public class DbHelper extends SQLiteOpenHelper {


    public DbHelper(Context context) {
        super(context, DbConstants.DB_NAME, null, DbConstants.DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("CREATE TABLE " + DbConstants.TABLE_NAME +
                " (" + DbConstants.USER_INFO_FIELD_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + DbConstants.USER_INFO_FIELD_NAME + " TEXT NOT NULL, "
                + DbConstants.USER_INFO_FIELD_SNAME + " TEXT, "
                + DbConstants.USER_INFO_FIELD_PHONE + " TEXT, "
                + DbConstants.USER_INFO_FIELD_EMAIL + " TEXT, "
                + DbConstants.USER_INFO_FIELD_BDAY + " INTEGER);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
