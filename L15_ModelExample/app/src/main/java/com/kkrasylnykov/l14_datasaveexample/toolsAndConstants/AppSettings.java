package com.kkrasylnykov.l14_datasaveexample.toolsAndConstants;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class AppSettings {
    private static final String KEY_BOOLEAN_IS_SHOW_TERMS = "KEY_BOOLEAN_IS_SHOW_TERMS";
    private static final String KEY_BOOLEAN_IS_EDIT_ACTIVITY_FINISH = "KEY_BOOLEAN_IS_EDIT_ACTIVITY_FINISH";

    private SharedPreferences m_SharedPreferences;

    public AppSettings(Context context){
        m_SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public boolean isShowTerms(){
        return m_SharedPreferences.getBoolean(KEY_BOOLEAN_IS_SHOW_TERMS, true);
    }

    public void setShowTerms(boolean bIsShow){
        SharedPreferences.Editor editor = m_SharedPreferences.edit();
        editor.putBoolean(KEY_BOOLEAN_IS_SHOW_TERMS, bIsShow);
        editor.commit();
    }

    public boolean isFinishEditActivity(){
        return m_SharedPreferences.getBoolean(KEY_BOOLEAN_IS_EDIT_ACTIVITY_FINISH, false);
    }

    public void setIsFinishEditActivity(boolean bIsFinish){
        SharedPreferences.Editor editor = m_SharedPreferences.edit();
        editor.putBoolean(KEY_BOOLEAN_IS_EDIT_ACTIVITY_FINISH, bIsFinish);
        editor.commit();
    }

}
