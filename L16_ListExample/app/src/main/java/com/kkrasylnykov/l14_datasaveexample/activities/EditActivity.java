package com.kkrasylnykov.l14_datasaveexample.activities;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kkrasylnykov.l14_datasaveexample.R;
import com.kkrasylnykov.l14_datasaveexample.db.DbHelper;
import com.kkrasylnykov.l14_datasaveexample.model.UserInfo;
import com.kkrasylnykov.l14_datasaveexample.model.engines.UserInfoEngine;
import com.kkrasylnykov.l14_datasaveexample.toolsAndConstants.AppSettings;
import com.kkrasylnykov.l14_datasaveexample.toolsAndConstants.DbConstants;

public class EditActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String KEY_ID = "KEY_ID";

    private EditText m_NameEditText;
    private EditText m_SNameEditText;
    private EditText m_PhoneEditText;
    private EditText m_EmailTextView;
    private EditText m_BDayTextView;

    private long m_nId = -1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        m_NameEditText = (EditText) findViewById(R.id.NameEditTextEditActivity);
        m_SNameEditText = (EditText) findViewById(R.id.SNameEditTextEditActivity);
        m_PhoneEditText = (EditText) findViewById(R.id.PhoneEditTextEditActivity);
        m_EmailTextView = (EditText) findViewById(R.id.EmailEditTextEditActivity);
        m_BDayTextView = (EditText) findViewById(R.id.BDayEditTextEditActivity);

        Button btnAdd = (Button) findViewById(R.id.buttonAddUserEditActivity);
        btnAdd.setOnClickListener(this);

        Intent intent = getIntent();
        if (intent!=null){
            Bundle bundle = intent.getExtras();
            if (bundle!=null){
                m_nId = bundle.getLong(KEY_ID, -1);
            }
        }

        if (m_nId>-1){
            UserInfoEngine userInfoEngine = new UserInfoEngine(this);
            UserInfo userInfo = userInfoEngine.getUserById(m_nId);
            if (userInfo!=null){
                m_NameEditText.setText(userInfo.getName());
                m_SNameEditText.setText(userInfo.getSName());
                m_PhoneEditText.setText(userInfo.getPhone());
                m_EmailTextView.setText(userInfo.getEmail());
                m_BDayTextView.setText(Long.toString(userInfo.getBDay()));

                btnAdd.setText("Update");
                Button btnRemove = (Button) findViewById(R.id.buttonRemoveUserEditActivity);
                btnRemove.setOnClickListener(this);
                btnRemove.setVisibility(View.VISIBLE);
            } else {
                Toast.makeText(this, "User not found!!!!", Toast.LENGTH_LONG).show();
                finish();
            }
        }
    }

    @Override
    public void onClick(View view) {
        UserInfoEngine userInfoEngine = new UserInfoEngine(this);
        switch (view.getId()){
            case R.id.buttonRemoveUserEditActivity:
                if (m_nId>-1){
                    userInfoEngine.removeUserById(m_nId);
                }
                finish();
                break;
            case R.id.buttonAddUserEditActivity:
                UserInfo userInfo = new UserInfo(m_NameEditText.getText().toString(),
                        m_SNameEditText.getText().toString(),
                        m_PhoneEditText.getText().toString(),
                        m_EmailTextView.getText().toString(),
                        Long.parseLong(m_BDayTextView.getText().toString()));

                if(!userInfo.validate()){
                    Toast.makeText(this, "Fields is not valid... bla... bla... bla...", Toast.LENGTH_LONG).show();
                    return;
                }
                if (m_nId>-1){
                    userInfo.setId(m_nId);
                    userInfoEngine.updateUser(userInfo);

                    finish();
                } else {
                    userInfoEngine.addUser(userInfo);

                    AppSettings appSettings = new AppSettings(this);
                    if (appSettings.isFinishEditActivity()){
                        finish();
                    } else {
                        m_NameEditText.setText("");
                        m_NameEditText.requestFocus();
                        m_SNameEditText.setText("");
                        m_PhoneEditText.setText("");
                        m_EmailTextView.setText("");
                        m_BDayTextView.setText("");
                    }
                }

                break;
        }
    }
}
