package com.kkrasylnykov.l14_datasaveexample.activities;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.kkrasylnykov.l14_datasaveexample.R;
import com.kkrasylnykov.l14_datasaveexample.adapters.UserInfoAdapter;
import com.kkrasylnykov.l14_datasaveexample.db.DbHelper;
import com.kkrasylnykov.l14_datasaveexample.model.UserInfo;
import com.kkrasylnykov.l14_datasaveexample.model.engines.UserInfoEngine;
import com.kkrasylnykov.l14_datasaveexample.toolsAndConstants.AppSettings;
import com.kkrasylnykov.l14_datasaveexample.toolsAndConstants.DbConstants;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener {

    private static final int REQUEST_CODE_TERMS_ACTIVITY = 101;

    //private LinearLayout m_conteynerLinearLayout;
    ListView m_ListView;
    UserInfoAdapter m_Adapter;
    private ArrayList<UserInfo> arrUsers = new ArrayList<>();

    private String m_strSearchText = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AppSettings appSettings = new AppSettings(this);
        if (appSettings.isShowTerms()){
            Intent intent = new Intent(this, TermsActivity.class);
            startActivityForResult(intent, REQUEST_CODE_TERMS_ACTIVITY);

            DbHelper dbHelper = new DbHelper(this);
            SQLiteDatabase db = dbHelper.getWritableDatabase();

            try {
                for (int i=0; i<100; i++){
                    UserInfo userInfo = new UserInfo("Kos"+ i,"Kras"+ i,"095" + i,
                            "test"+ i+"@g.m",56+i*5);

                    db.insert(DbConstants.TABLE_NAME, null, userInfo.getContentValues());
                }
            } finally {
                db.close();
            }
        }

        EditText editText = (EditText) findViewById(R.id.searchEditTextMainActivity);
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                m_strSearchText = charSequence.toString();
                updateInfo();
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        //m_conteynerLinearLayout = (LinearLayout) findViewById(R.id.contayner);
        m_ListView = (ListView) findViewById(R.id.listViewMainActivity);
        m_Adapter = new UserInfoAdapter(arrUsers);
        m_ListView.setOnItemClickListener(this);
        m_ListView.setAdapter(m_Adapter);
        findViewById(R.id.buttonAddUserMainActivity).setOnClickListener(this);
        findViewById(R.id.buttonRemoveAllUserMainActivity).setOnClickListener(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_TERMS_ACTIVITY && resultCode!=RESULT_OK){
            finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateInfo();
    }

    private void updateInfo(){
        arrUsers.clear();
        UserInfoEngine userInfoEngine = new UserInfoEngine(this);
        if(m_strSearchText.isEmpty()){
            arrUsers.addAll(userInfoEngine.getAllUsers());
        } else {
            arrUsers.addAll(userInfoEngine.getUsersBySearchString(m_strSearchText));
        }

        m_Adapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.buttonAddUserMainActivity:
                Intent intent = new Intent(this, EditActivity.class);
                startActivity(intent);
                break;
            case R.id.buttonRemoveAllUserMainActivity:
                UserInfoEngine userInfoEngine = new UserInfoEngine(this);
                userInfoEngine.removeAll();
                break;
        }

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Intent intent = new Intent(this, EditActivity.class);
        intent.putExtra(EditActivity.KEY_ID, l);
        startActivity(intent);
    }
}
