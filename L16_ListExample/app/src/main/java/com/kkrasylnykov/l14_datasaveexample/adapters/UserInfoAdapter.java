package com.kkrasylnykov.l14_datasaveexample.adapters;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kkrasylnykov.l14_datasaveexample.R;
import com.kkrasylnykov.l14_datasaveexample.model.UserInfo;

import java.util.ArrayList;

public class UserInfoAdapter extends BaseAdapter {

    ArrayList<UserInfo> m_arrUsers;

    public UserInfoAdapter(ArrayList<UserInfo> arrUsers){
        m_arrUsers = arrUsers;
    }

    @Override
    public int getCount() {
        return m_arrUsers.size();
    }

    @Override
    public Object getItem(int i) {
        return m_arrUsers.get(i);
    }

    @Override
    public long getItemId(int i) {
        return ((UserInfo)getItem(i)).getId();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        Log.d("devcpp", "getView -> view -> " + view + " -> " + i);
        ViewHolder holder;
        if (view==null){
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_user_info, viewGroup, false);
            holder = new ViewHolder();
            holder.fullnameTextView = (TextView) view.findViewById(R.id.fullnameTextViewItem);
            holder.phoneTextView = (TextView) view.findViewById(R.id.phoneTextViewItem);
            holder.emailTextView = (TextView) view.findViewById(R.id.emailTextViewItem);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        UserInfo userInfo = (UserInfo)getItem(i);
        holder.fullnameTextView.setText(userInfo.getName() + " " + userInfo.getSName());
        holder.phoneTextView.setText(userInfo.getPhone());
        holder.emailTextView.setText(userInfo.getEmail());
        return view;
    }

    private class ViewHolder {
        TextView fullnameTextView;
        TextView phoneTextView;
        TextView emailTextView;
    }
}
