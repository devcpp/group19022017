package com.kkrasylnykov.l14_datasaveexample.model;


import android.content.ContentValues;

public abstract class BaseEntity {

    private long m_nId = -1;

    public long getId() {
        return m_nId;
    }

    public void setId(long m_nId) {
        this.m_nId = m_nId;
    }

    public abstract ContentValues getContentValues();

    protected static boolean isLong(String s) {
        try {
            Long.parseLong(s);
        } catch(NumberFormatException e) {
            return false;
        } catch(NullPointerException e) {
            return false;
        }
        // only got here if we didn't return false
        return true;
    }
}
