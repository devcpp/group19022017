package com.kkrasylnykov.l14_datasaveexample.model;

import android.content.ContentValues;
import android.database.Cursor;

import com.kkrasylnykov.l14_datasaveexample.toolsAndConstants.DbConstants;

public class UserInfo extends BaseEntity {
    private String m_strName;
    private String m_strSName;
    private String m_strPhone;
    private String m_strEmail;
    private long m_nBDay;

    public UserInfo(String m_strName, String m_strSName, String m_strPhone, String m_strEmail, long m_nBDay) {
        this.m_strName = m_strName;
        this.m_strSName = m_strSName;
        this.m_strPhone = m_strPhone;
        this.m_strEmail = m_strEmail;
        this.m_nBDay = m_nBDay;
    }

    public UserInfo(Cursor cursor){
        setId(cursor.getLong(cursor.getColumnIndex(DbConstants.USER_INFO_FIELD_ID)));
        this.m_strName = cursor.getString(cursor.getColumnIndex(DbConstants.USER_INFO_FIELD_NAME));
        this.m_strSName = cursor.getString(cursor.getColumnIndex(DbConstants.USER_INFO_FIELD_SNAME));
        this.m_strPhone = cursor.getString(cursor.getColumnIndex(DbConstants.USER_INFO_FIELD_PHONE));
        this.m_strEmail = cursor.getString(cursor.getColumnIndex(DbConstants.USER_INFO_FIELD_EMAIL));
        this.m_nBDay = cursor.getLong(cursor.getColumnIndex(DbConstants.USER_INFO_FIELD_BDAY));
    }

    public String getName() {
        return m_strName;
    }

    public void setName(String m_strName) {
        this.m_strName = m_strName;
    }

    public String getSName() {
        return m_strSName;
    }

    public void setSName(String m_strSName) {
        this.m_strSName = m_strSName;
    }

    public String getPhone() {
        return m_strPhone;
    }

    public void setPhone(String m_strPhone) {
        this.m_strPhone = m_strPhone;
    }

    public String getEmail() {
        return m_strEmail;
    }

    public void setEmail(String m_strEmail) {
        this.m_strEmail = m_strEmail;
    }

    public long getBDay() {
        return m_nBDay;
    }

    public void setBDay(long m_nBDay) {
        this.m_nBDay = m_nBDay;
    }

    public boolean validate(){
        boolean bResult = !"".equals(getName()) && getBDay()>0;

        return bResult;
    }

    @Override
    public ContentValues getContentValues() {
        ContentValues values = new ContentValues();
        values.put(DbConstants.USER_INFO_FIELD_NAME, getName());
        values.put(DbConstants.USER_INFO_FIELD_SNAME, getSName());
        values.put(DbConstants.USER_INFO_FIELD_PHONE, getPhone());
        values.put(DbConstants.USER_INFO_FIELD_EMAIL, getEmail());
        values.put(DbConstants.USER_INFO_FIELD_BDAY, getBDay());
        return values;
    }
}
