package com.kkrasylnykov.l14_datasaveexample.model.engines;

import android.content.Context;

import com.kkrasylnykov.l14_datasaveexample.model.UserInfo;
import com.kkrasylnykov.l14_datasaveexample.model.wrappers.dbWrappers.UserInfoDBWrapper;

import java.util.ArrayList;

public class UserInfoEngine extends BaseEngine {

    public UserInfoEngine(Context m_context) {
        super(m_context);
    }

    public ArrayList<UserInfo> getAllUsers(){
        UserInfoDBWrapper userInfoDBWrapper = new UserInfoDBWrapper(getContext());
        return userInfoDBWrapper.getAllUsers();
    }

    public ArrayList<UserInfo> getUsersBySearchString(String strSearch){
        UserInfoDBWrapper userInfoDBWrapper = new UserInfoDBWrapper(getContext());
        return userInfoDBWrapper.getUsersBySearchString(strSearch);
    }

    public UserInfo getUserById(long nId){
        UserInfoDBWrapper userInfoDBWrapper = new UserInfoDBWrapper(getContext());
        return userInfoDBWrapper.getUserById(nId);
    }

    public void addUser(UserInfo userInfo){
        UserInfoDBWrapper userInfoDBWrapper = new UserInfoDBWrapper(getContext());
        userInfoDBWrapper.addUser(userInfo);
    }

    public void updateUser(UserInfo userInfo){
        UserInfoDBWrapper userInfoDBWrapper = new UserInfoDBWrapper(getContext());
        userInfoDBWrapper.updateUser(userInfo);
    }

    public void removeUser(UserInfo userInfo){
        removeUserById(userInfo.getId());
    }

    public void removeUserById(long nId){
        UserInfoDBWrapper userInfoDBWrapper = new UserInfoDBWrapper(getContext());
        userInfoDBWrapper.removeUserById(nId);
    }

    public void removeAll(){
        UserInfoDBWrapper userInfoDBWrapper = new UserInfoDBWrapper(getContext());
        userInfoDBWrapper.removeAll();
    }
}
