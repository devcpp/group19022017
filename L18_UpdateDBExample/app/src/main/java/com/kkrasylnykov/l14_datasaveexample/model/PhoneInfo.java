package com.kkrasylnykov.l14_datasaveexample.model;

import android.content.ContentValues;
import android.database.Cursor;

import com.kkrasylnykov.l14_datasaveexample.toolsAndConstants.DbConstants;

public class PhoneInfo extends BaseEntity {
    private long m_nUserId = -1;
    private String m_strPhone;

    public PhoneInfo(long m_nUserId, String m_strPhone) {
        this.m_nUserId = m_nUserId;
        this.m_strPhone = m_strPhone;
    }

    public PhoneInfo(Cursor cursor){
        setId(cursor.getLong(cursor.getColumnIndex(DbConstants.DB_V2.TABLE_PHONES.FIELD_ID)));
        this.m_nUserId =cursor.getLong(cursor.getColumnIndex(DbConstants.DB_V2.TABLE_PHONES.FIELD_USER_ID));
        this.m_strPhone = cursor.getString(cursor.getColumnIndex(DbConstants.DB_V2.TABLE_PHONES.FIELD_PHONE));
    }

    public long getUserId() {
        return m_nUserId;
    }

    public void setUserId(long m_nUserId) {
        this.m_nUserId = m_nUserId;
    }

    public String getPhone() {
        return m_strPhone;
    }

    public void setPhone(String m_strPhone) {
        this.m_strPhone = m_strPhone;
    }

    @Override
    public ContentValues getContentValues() {
        ContentValues values = new ContentValues();
        values.put(DbConstants.DB_V2.TABLE_PHONES.FIELD_USER_ID, getUserId());
        values.put(DbConstants.DB_V2.TABLE_PHONES.FIELD_PHONE, getPhone());
        return values;
    }
}
