package com.kkrasylnykov.l14_datasaveexample.model.engines;

import android.content.Context;

import com.kkrasylnykov.l14_datasaveexample.model.PhoneInfo;
import com.kkrasylnykov.l14_datasaveexample.model.UserInfo;
import com.kkrasylnykov.l14_datasaveexample.model.wrappers.dbWrappers.UserInfoDBWrapper;

import java.util.ArrayList;

public class UserInfoEngine extends BaseEngine {

    public UserInfoEngine(Context m_context) {
        super(m_context);
    }

    public ArrayList<UserInfo> getAllUsers(){
        UserInfoDBWrapper userInfoDBWrapper = new UserInfoDBWrapper(getContext());
        ArrayList<UserInfo> arrData = userInfoDBWrapper.getAllUsers();
        PhonesInfoEngine phonesInfoEngine = new PhonesInfoEngine(getContext());
        for(UserInfo item:arrData){
            item.setPhones(phonesInfoEngine.getPhonesByUserId(item.getId()));
        }
        return arrData;
    }

    public ArrayList<UserInfo> getUsersBySearchString(String strSearch){
        UserInfoDBWrapper userInfoDBWrapper = new UserInfoDBWrapper(getContext());
        ArrayList<UserInfo> arrData = userInfoDBWrapper.getUsersBySearchString(strSearch);
        PhonesInfoEngine phonesInfoEngine = new PhonesInfoEngine(getContext());
        for(UserInfo item:arrData){
            item.setPhones(phonesInfoEngine.getPhonesByUserId(item.getId()));
        }
        return arrData;
    }

    public UserInfo getUserById(long nId){
        UserInfoDBWrapper userInfoDBWrapper = new UserInfoDBWrapper(getContext());
        UserInfo userInfo = userInfoDBWrapper.getUserById(nId);
        PhonesInfoEngine phonesInfoEngine = new PhonesInfoEngine(getContext());
        userInfo.setPhones(phonesInfoEngine.getPhonesByUserId(userInfo.getId()));
        return userInfo;
    }

    public void addUser(UserInfo userInfo){
        UserInfoDBWrapper userInfoDBWrapper = new UserInfoDBWrapper(getContext());
        long id = userInfoDBWrapper.addUser(userInfo);
        PhonesInfoEngine phonesInfoEngine = new PhonesInfoEngine(getContext());
        for(PhoneInfo phoneInfo:userInfo.getPhones()){
            phoneInfo.setUserId(id);
            phonesInfoEngine.addPhone(phoneInfo);
        }
    }

    public void updateUser(UserInfo userInfo){
        UserInfoDBWrapper userInfoDBWrapper = new UserInfoDBWrapper(getContext());
        userInfoDBWrapper.updateUser(userInfo);
        PhonesInfoEngine phonesInfoEngine = new PhonesInfoEngine(getContext());
        for(PhoneInfo phoneInfo:userInfo.getPhones()){
            if (phoneInfo.getUserId()==-1){
                phoneInfo.setUserId(userInfo.getId());
                phonesInfoEngine.addPhone(phoneInfo);
            } else {
                phonesInfoEngine.updatePhone(phoneInfo);
            }
        }
    }

    public void removeUser(UserInfo userInfo){
        removeUserById(userInfo.getId());
    }

    public void removeUserById(long nId){
        UserInfoDBWrapper userInfoDBWrapper = new UserInfoDBWrapper(getContext());
        userInfoDBWrapper.removeUserById(nId);
        PhonesInfoEngine phonesInfoEngine = new PhonesInfoEngine(getContext());
        phonesInfoEngine.removePhonesByUserId(nId);
    }

    public void removeAll(){
        UserInfoDBWrapper userInfoDBWrapper = new UserInfoDBWrapper(getContext());
        userInfoDBWrapper.removeAll();
        PhonesInfoEngine phonesInfoEngine = new PhonesInfoEngine(getContext());
        phonesInfoEngine.removeAll();
    }
}
