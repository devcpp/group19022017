package com.kkrasylnykov.l14_datasaveexample.model.wrappers.dbWrappers;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.kkrasylnykov.l14_datasaveexample.model.PhoneInfo;
import com.kkrasylnykov.l14_datasaveexample.model.UserInfo;
import com.kkrasylnykov.l14_datasaveexample.toolsAndConstants.DbConstants;

import java.util.ArrayList;

public class PhoneInfoDBWrapper extends BaseDBWrapper {

    public PhoneInfoDBWrapper(Context context) {
        super(context, DbConstants.DB_V2.TABLE_PHONES.TABLE_NAME);
    }

    public ArrayList<PhoneInfo> getPhonesByUserId(long nId){
        ArrayList<PhoneInfo> arrResult = new ArrayList<>();
        SQLiteDatabase db = getReadable();
        String strRequest = DbConstants.DB_V2.TABLE_PHONES.FIELD_USER_ID + "=?";
        String arrArgs[] = new String[]{Long.toString(nId)};
        Cursor cursor = db.query(getTableName(),null,strRequest,arrArgs,null,null,null);
        try{
            if (cursor!=null && cursor.moveToFirst()){
                do{
                    arrResult.add(new PhoneInfo(cursor));
                }while (cursor.moveToNext());
            }
        } finally {
            if (cursor!=null){
                cursor.close();
            }
            db.close();
        }
        return arrResult;
    }

    public void addPhone(PhoneInfo item){
        SQLiteDatabase db = getWritable();
        db.insert(getTableName(), null, item.getContentValues());
        db.close();
    }

    public void updateUser(PhoneInfo item){
        SQLiteDatabase db = getWritable();
        String strRequest = DbConstants.DB_V2.TABLE_PHONES.FIELD_ID + "=?";
        String arrArgs[] = new String[]{Long.toString(item.getId())};
        db.update(getTableName(), item.getContentValues(), strRequest, arrArgs);
        db.close();
    }

    public void removePhoneById(long nId){
        SQLiteDatabase db = getWritable();
        String strRequest = DbConstants.DB_V2.TABLE_PHONES.FIELD_ID + "=?";
        String arrArgs[] = new String[]{Long.toString(nId)};
        db.delete(getTableName(),strRequest,arrArgs);
        db.close();
    }

    public void removePhonesByUserId(long nUserId){
        SQLiteDatabase db = getWritable();
        String strRequest = DbConstants.DB_V2.TABLE_PHONES.FIELD_USER_ID + "=?";
        String arrArgs[] = new String[]{Long.toString(nUserId)};
        db.delete(getTableName(),strRequest,arrArgs);
        db.close();
    }

    public void removeAll(){
        SQLiteDatabase db = getWritable();
        db.delete(getTableName(),null,null);
        db.close();
    }
}
