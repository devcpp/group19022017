package com.kkrasylnykov.l14_datasaveexample.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.kkrasylnykov.l14_datasaveexample.R;
import com.kkrasylnykov.l14_datasaveexample.model.PhoneInfo;
import com.kkrasylnykov.l14_datasaveexample.model.UserInfo;
import com.kkrasylnykov.l14_datasaveexample.model.engines.UserInfoEngine;
import com.kkrasylnykov.l14_datasaveexample.toolsAndConstants.AppSettings;

import java.util.ArrayList;

public class EditActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String KEY_ID = "KEY_ID";

    private EditText m_NameEditText;
    private EditText m_SNameEditText;
    private EditText m_EmailTextView;
    private EditText m_BDayTextView;

    private LinearLayout m_phoneLinearLayout;

    private UserInfo m_UserInfo = null;
    private LinearLayout.LayoutParams m_paramsPhoneInfoView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        m_NameEditText = (EditText) findViewById(R.id.NameEditTextEditActivity);
        m_SNameEditText = (EditText) findViewById(R.id.SNameEditTextEditActivity);
        m_EmailTextView = (EditText) findViewById(R.id.EmailEditTextEditActivity);
        m_BDayTextView = (EditText) findViewById(R.id.BDayEditTextEditActivity);

        m_phoneLinearLayout = (LinearLayout) findViewById(R.id.linearLayoutPhonesEditActivity);

        Button btnAdd = (Button) findViewById(R.id.buttonAddUserEditActivity);
        btnAdd.setOnClickListener(this);

        Intent intent = getIntent();
        if (intent!=null){
            Bundle bundle = intent.getExtras();
            if (bundle!=null){
                m_UserInfo = (UserInfo) bundle.get(KEY_ID);
            }
        }

        m_paramsPhoneInfoView =
                new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);
        if (m_UserInfo!=null){
            m_NameEditText.setText(m_UserInfo.getName());
            m_SNameEditText.setText(m_UserInfo.getSName());
            m_EmailTextView.setText(m_UserInfo.getEmail());
            m_BDayTextView.setText(Long.toString(m_UserInfo.getBDay()));

            ArrayList<PhoneInfo> arrPhones = m_UserInfo.getPhones();
            if (arrPhones!=null){
                for (PhoneInfo phoneInfo:arrPhones){
                    addToConteynerPhoneEditText(this, phoneInfo);
                }
            }

            btnAdd.setText("Update");
            Button btnRemove = (Button) findViewById(R.id.buttonRemoveUserEditActivity);
            btnRemove.setOnClickListener(this);
            btnRemove.setVisibility(View.VISIBLE);
        }

        addToConteynerPhoneEditText(this);

        Button addPhoneButton = (Button) findViewById(R.id.addPhoneButtonEditActivity);
        addPhoneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addToConteynerPhoneEditText(EditActivity.this);
            }
        });
    }
    private void addToConteynerPhoneEditText(Context context){
        addToConteynerPhoneEditText(context, null);
    }

    private void addToConteynerPhoneEditText(Context context, PhoneInfo phoneInfo){
        EditText editPhoneInfo = new EditText(context);
        editPhoneInfo.setLayoutParams(m_paramsPhoneInfoView);
        editPhoneInfo.setHint("Enter Phone");
        if (phoneInfo!=null){
            editPhoneInfo.setText(phoneInfo.getPhone());
            editPhoneInfo.setTag(phoneInfo.getId());
        } else {
            editPhoneInfo.setTag(-1);
        }
        m_phoneLinearLayout.addView(editPhoneInfo);
    }

    @Override
    public void onClick(View view) {
        UserInfoEngine userInfoEngine = new UserInfoEngine(this);
        switch (view.getId()){
            case R.id.buttonRemoveUserEditActivity:
                if (m_UserInfo!=null){
                    userInfoEngine.removeUserById(m_UserInfo.getId());
                }
                finish();
                break;
            case R.id.buttonAddUserEditActivity:
                UserInfo userInfo = new UserInfo(m_NameEditText.getText().toString(),
                        m_SNameEditText.getText().toString(),
                        m_EmailTextView.getText().toString(),
                        Long.parseLong(m_BDayTextView.getText().toString()));

                if(!userInfo.validate()){
                    Toast.makeText(this, "Fields is not valid... bla... bla... bla...", Toast.LENGTH_LONG).show();
                    return;
                }

                ArrayList<PhoneInfo> arrData = new ArrayList<>();
                int nCount = m_phoneLinearLayout.getChildCount();
                long nUserId = m_UserInfo==null?-1:m_UserInfo.getId();
                for (int i=0; i<nCount; i++){
                    View v = m_phoneLinearLayout.getChildAt(i);
                    if (v instanceof EditText){
                        EditText phoneInfoEditText = (EditText) v;
                        String strPhone = phoneInfoEditText.getText().toString();
                        Object tag = phoneInfoEditText.getTag();
                        long nPhoneId = -1;
                        if (tag!=null && tag instanceof Long){
                            nPhoneId = (Long) tag;
                        }

                        if (nPhoneId==-1 && strPhone.isEmpty()){
                            continue;
                        }
                        PhoneInfo phoneInfo = new PhoneInfo(nUserId, strPhone);
                        phoneInfo.setId(nPhoneId);

                        arrData.add(phoneInfo);
                    }
                }

                userInfo.setId(nUserId);
                userInfo.setPhones(arrData);

                if (userInfo.getId()>-1){;
                    userInfoEngine.updateUser(userInfo);

                    finish();
                } else {
                    userInfoEngine.addUser(userInfo);

                    AppSettings appSettings = new AppSettings(this);
                    if (appSettings.isFinishEditActivity()){
                        finish();
                    } else {
                        m_NameEditText.setText("");
                        m_NameEditText.requestFocus();
                        m_SNameEditText.setText("");
                        m_phoneLinearLayout.removeAllViews();
                        addToConteynerPhoneEditText(this);
                        m_EmailTextView.setText("");
                        m_BDayTextView.setText("");
                    }
                }

                break;
        }
    }
}
