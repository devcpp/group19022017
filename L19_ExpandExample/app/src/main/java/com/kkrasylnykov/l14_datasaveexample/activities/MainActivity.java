package com.kkrasylnykov.l14_datasaveexample.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

import com.kkrasylnykov.l14_datasaveexample.R;
import com.kkrasylnykov.l14_datasaveexample.adapters.UserInfoRecyclerAdapter;
import com.kkrasylnykov.l14_datasaveexample.model.UserInfo;
import com.kkrasylnykov.l14_datasaveexample.model.engines.UserInfoEngine;
import com.kkrasylnykov.l14_datasaveexample.toolsAndConstants.AppSettings;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, UserInfoRecyclerAdapter.OnClickUserInfoItem{

    private static final int REQUEST_CODE_TERMS_ACTIVITY = 101;

    RecyclerView m_RecyclerView;
    UserInfoRecyclerAdapter m_Adapter;
    private ArrayList<UserInfo> arrUsers = new ArrayList<>();

    private String m_strSearchText = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AppSettings appSettings = new AppSettings(this);
        if (appSettings.isShowTerms()){
            Intent intent = new Intent(this, TermsActivity.class);
            startActivityForResult(intent, REQUEST_CODE_TERMS_ACTIVITY);
        }

        EditText editText = (EditText) findViewById(R.id.searchEditTextMainActivity);
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                m_strSearchText = charSequence.toString();
                updateInfo();
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        m_RecyclerView = (RecyclerView) findViewById(R.id.recyclerViewMainActivity);
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        m_RecyclerView.setLayoutManager(layoutManager);
        m_Adapter = new UserInfoRecyclerAdapter(arrUsers);
        m_Adapter.setOnClickUserInfoItem(this);
        m_RecyclerView.setAdapter(m_Adapter);
        findViewById(R.id.buttonAddUserMainActivity).setOnClickListener(this);
        findViewById(R.id.buttonRemoveAllUserMainActivity).setOnClickListener(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_TERMS_ACTIVITY && resultCode!=RESULT_OK){
            finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateInfo();
    }

    private void updateInfo(){
        UserInfoEngine userInfoEngine = new UserInfoEngine(this);
        if(m_strSearchText.isEmpty()){
            m_Adapter.updateData(userInfoEngine.getAllUsers());
        } else {
            m_Adapter.updateData(userInfoEngine.getUsersBySearchString(m_strSearchText));
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.buttonAddUserMainActivity:
                Intent intent = new Intent(this, EditActivity.class);
                startActivity(intent);
                break;
            case R.id.buttonRemoveAllUserMainActivity:
                UserInfoEngine userInfoEngine = new UserInfoEngine(this);
                userInfoEngine.removeAll();
                break;
        }

    }

    @Override
    public void onClickUserInfoItem(UserInfo userInfo) {
        Intent intent = new Intent(this, EditActivity.class);
        intent.putExtra(EditActivity.KEY_ID, userInfo);
        startActivity(intent);
    }
}
