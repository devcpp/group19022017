package com.kkrasylnykov.l14_datasaveexample.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.kkrasylnykov.l14_datasaveexample.R;
import com.kkrasylnykov.l14_datasaveexample.toolsAndConstants.AppSettings;

public class TermsActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms);

        findViewById(R.id.yesBtnTermsActivity).setOnClickListener(this);
        findViewById(R.id.noBtnTermsActivity).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.yesBtnTermsActivity:
                AppSettings appSettings = new AppSettings(this);
                appSettings.setShowTerms(false);
                setResult(RESULT_OK);
                break;
            case R.id.noBtnTermsActivity:
                setResult(RESULT_CANCELED);
                break;
        }
        finish();
    }
}
