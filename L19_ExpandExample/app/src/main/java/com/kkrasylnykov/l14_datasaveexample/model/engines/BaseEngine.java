package com.kkrasylnykov.l14_datasaveexample.model.engines;

import android.content.Context;

public abstract class BaseEngine {
    private Context m_context;

    public BaseEngine(Context m_context) {
        this.m_context = m_context;
    }

    public Context getContext() {
        return m_context;
    }
}
