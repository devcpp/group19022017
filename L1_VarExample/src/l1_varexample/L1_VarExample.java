package l1_varexample;

import java.util.Scanner;

public class L1_VarExample {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = 10;
        int b = 5;
        int nResultatSumm = n + b;
        float f = (float)1.6; //Not TRUE
        float f2 = 1.6f; // TRUE
        float fDel = ((float)b)/n;
        byte nTestReadByte = scan.nextByte();
        short nTestReadShort = scan.nextShort();
        float fTestReadFloat = scan.nextFloat();
        int nTestReadInt = scan.nextInt();
        
        float fTest = fTestReadFloat/ nTestReadByte;       
        int nTest = nTestReadInt / nTestReadShort;
        System.out.println("Summ: " + fTest);
    }

}
