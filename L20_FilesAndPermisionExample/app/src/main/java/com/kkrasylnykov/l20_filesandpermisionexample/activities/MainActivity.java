package com.kkrasylnykov.l20_filesandpermisionexample.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.kkrasylnykov.l20_filesandpermisionexample.R;
import com.kkrasylnykov.l20_filesandpermisionexample.adapters.FileListAdapter;

import java.io.File;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private static final int REQUEST_PERMISSION_ON_READ_FILE = 10;


    private ListView m_ListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        m_ListView = (ListView) findViewById(R.id.listView);

        if(ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_PERMISSION_ON_READ_FILE);
        } else {
            showFilesList();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode== REQUEST_PERMISSION_ON_READ_FILE){
            if(ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                    !=PackageManager.PERMISSION_GRANTED){
                Toast.makeText(this, "Get read storage permission", Toast.LENGTH_LONG).show();
                finish();
            } else {
                showFilesList();
            }
        }
    }

    private void showFilesList(){
        final ArrayList<File> arrData = getPicFiles(Environment.getExternalStorageDirectory());

        FileListAdapter adapter = new FileListAdapter();
        adapter.updateData(arrData);
        m_ListView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        m_ListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this, ViewActivity.class);
                intent.putExtra(ViewActivity.KEY_PATH, arrData.get(position).getAbsolutePath());
                startActivity(intent);
            }
        });
    }

    private ArrayList<File> getPicFiles(File file){
        ArrayList<File> files = new ArrayList<>();
        if (file.isDirectory()){
            File[] data = file.listFiles();
            if (data!=null){
                for (File curFile:data){
                    files.addAll(getPicFiles(curFile));
                }
            }
        } else {
            if (file.getName().endsWith(".png") || file.getName().endsWith(".jpg") || file.getName().endsWith(".jpeg")){
                files.add(file);
            }
        }
        return files;
    }
}
