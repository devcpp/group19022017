package com.kkrasylnykov.l20_filesandpermisionexample.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;

import com.kkrasylnykov.l20_filesandpermisionexample.R;

import java.io.File;

public class ViewActivity extends AppCompatActivity {

    public static final String KEY_PATH = "KEY_PATH";

    private ImageView imageView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view);

        Bundle bundle = getIntent().getExtras();
        String strPath = "";
        if (bundle!=null){
            strPath = bundle.getString(KEY_PATH, "");
        }

        if (strPath.isEmpty()){
            finish();
        }

        imageView = (ImageView) findViewById(R.id.ImageView);
        Log.d("devcpp","strPath -> " + strPath);

        File imgFile = new  File(strPath);

        if(imgFile.exists()){
            Bitmap bitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());

            imageView.setImageBitmap(bitmap);
        }

    }
}
