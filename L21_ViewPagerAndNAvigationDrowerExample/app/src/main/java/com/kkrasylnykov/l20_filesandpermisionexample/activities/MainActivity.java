package com.kkrasylnykov.l20_filesandpermisionexample.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.kkrasylnykov.l20_filesandpermisionexample.R;
import com.kkrasylnykov.l20_filesandpermisionexample.adapters.FileListAdapter;

import java.io.File;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private static final int REQUEST_PERMISSION_ON_READ_FILE = 10;


    private ListView m_ListView;
    private Toolbar mToolbar;

    private DrawerLayout m_DrawerLayout = null;
    private View m_NavigationDrawer = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        m_DrawerLayout = (DrawerLayout) findViewById(R.id.DrawerLayoutMainActivity);
        m_NavigationDrawer = findViewById(R.id.NavigationDrawer);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        getSupportActionBar().setTitle("testCode");

        Drawable menuIconDrawable = ContextCompat.getDrawable(this, R.drawable.ic_menu_black_24dp);
        menuIconDrawable.setColorFilter(ContextCompat.getColor(this, android.R.color.white), PorterDuff.Mode.SRC_ATOP);

        mToolbar.setNavigationIcon(menuIconDrawable);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_DrawerLayout.openDrawer(m_NavigationDrawer);
            }
        });

        Drawable menuRightIconDrawable = ContextCompat.getDrawable(this, R.drawable.ic_more_vert_black_24dp);
        menuRightIconDrawable.setColorFilter(ContextCompat.getColor(this, android.R.color.white), PorterDuff.Mode.SRC_ATOP);
        mToolbar.setOverflowIcon(menuRightIconDrawable);

        m_ListView = (ListView) findViewById(R.id.listView);

        if(ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_PERMISSION_ON_READ_FILE);
        } else {
            showFilesList();
        }
    }

    final static int MENU_ITEM_TIMER = 101;
    final static int MENU_ITEM_SETTINGS = 102;
    final static int MENU_ITEM_DELETE = 103;
    final static int MENU_ITEM_HELP = 104;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuItem itemTime = menu.add(0, MENU_ITEM_TIMER,0,"Timer");
        Drawable drawableIconTimer = ContextCompat.getDrawable(this, R.drawable.ic_timer_black_24dp);
        drawableIconTimer.setColorFilter(ContextCompat.getColor(this, android.R.color.white), PorterDuff.Mode.SRC_ATOP);
        itemTime.setIcon(drawableIconTimer);
        itemTime.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

        MenuItem itemSettings = menu.add(1, MENU_ITEM_SETTINGS,1,"Settings");
        Drawable drawableSettings = ContextCompat.getDrawable(this, R.drawable.ic_settings_black_24dp);
        drawableSettings.setColorFilter(ContextCompat.getColor(this, android.R.color.white), PorterDuff.Mode.SRC_ATOP);
        itemSettings.setIcon(drawableSettings);
        itemSettings.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

        MenuItem itemDelete = menu.add(2, MENU_ITEM_DELETE,2,"Delete");
        Drawable drawableDelete = ContextCompat.getDrawable(this, R.drawable.ic_delete_black_24dp);
        drawableDelete.setColorFilter(ContextCompat.getColor(this, android.R.color.white), PorterDuff.Mode.SRC_ATOP);
        itemDelete.setIcon(drawableDelete);
        itemDelete.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

        MenuItem itemHelp = menu.add(3, MENU_ITEM_HELP,3,"Help");;
        itemHelp.setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case MENU_ITEM_TIMER:
                Toast.makeText(this,"MENU_ITEM_TIMER",Toast.LENGTH_LONG).show();
                break;
            case MENU_ITEM_SETTINGS:
                Toast.makeText(this,"MENU_ITEM_SETTINGS",Toast.LENGTH_LONG).show();
                break;
            case MENU_ITEM_DELETE:
                Toast.makeText(this,"MENU_ITEM_DELETE",Toast.LENGTH_LONG).show();
                break;
            case MENU_ITEM_HELP:
                Toast.makeText(this,"MENU_ITEM_HELP",Toast.LENGTH_LONG).show();
                break;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode== REQUEST_PERMISSION_ON_READ_FILE){
            if(ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                    !=PackageManager.PERMISSION_GRANTED){
                Toast.makeText(this, "Get read storage permission", Toast.LENGTH_LONG).show();
                finish();
            } else {
                showFilesList();
            }
        }
    }

    private void showFilesList(){
        final ArrayList<File> arrData = getPicFiles(Environment.getExternalStorageDirectory());

        FileListAdapter adapter = new FileListAdapter();
        adapter.updateData(arrData);
        m_ListView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        m_ListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ArrayList<String> arrPaths = new ArrayList<String>();
                for(File curFile:arrData){
                    arrPaths.add(curFile.getAbsolutePath());
                }
                Intent intent = new Intent(MainActivity.this, ViewActivity.class);
                intent.putExtra(ViewActivity.KEY_PATHS, arrPaths);
                intent.putExtra(ViewActivity.POSITION_ITEM, position);
                startActivity(intent);
            }
        });
    }

    private ArrayList<File> getPicFiles(File file){
        ArrayList<File> files = new ArrayList<>();
        if (file.isDirectory()){
            File[] data = file.listFiles();
            if (data!=null){
                for (File curFile:data){
                    files.addAll(getPicFiles(curFile));
                }
            }
        } else {
            if (file.getName().endsWith(".png") || file.getName().endsWith(".jpg") || file.getName().endsWith(".jpeg")){
                files.add(file);
            }
        }
        return files;
    }
}
