package com.kkrasylnykov.l20_filesandpermisionexample.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.kkrasylnykov.l20_filesandpermisionexample.R;
import com.kkrasylnykov.l20_filesandpermisionexample.adapters.ImageViewPagerAdapter;

import java.util.ArrayList;

public class ViewActivity extends AppCompatActivity {

    public static final String KEY_PATHS = "KEY_PATHS";
    public static final String POSITION_ITEM = "POSITION_ITEM";

    //private ImageView imageView;
    ViewPager viewPager;
    ImageViewPagerAdapter adapter;
    Toast toast;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view);

        Bundle bundle = getIntent().getExtras();
        int nPosition = 0;
        ArrayList<String> arrPaths = new ArrayList<>();
        if (bundle!=null){
            arrPaths = bundle.getStringArrayList(KEY_PATHS);
            nPosition = bundle.getInt(POSITION_ITEM, 0);
        }

        if (arrPaths.isEmpty()){
            Toast.makeText(this, "Paths list is empty!", Toast.LENGTH_LONG).show();
            finish();
        }

        viewPager = (ViewPager) findViewById(R.id.ImageViewPager);
        adapter = new ImageViewPagerAdapter(getSupportFragmentManager(), arrPaths);
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(nPosition);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (toast!=null){
                    toast.cancel();
                    toast = null;
                }
                int nSize = adapter.getCount();
                int nPosition = position + 1;
                toast = Toast.makeText(ViewActivity.this, nPosition + " of " + nSize, Toast.LENGTH_SHORT);
                toast.show();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        /*imageView = (ImageView) findViewById(R.id.ImageView);
        Log.d("devcpp","strPath -> " + strPath);

        File imgFile = new  File(strPath);

        if(imgFile.exists()){
            Bitmap bitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());

            imageView.setImageBitmap(bitmap);
        }*/

    }
}
