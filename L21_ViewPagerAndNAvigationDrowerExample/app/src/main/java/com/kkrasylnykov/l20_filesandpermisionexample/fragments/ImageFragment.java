package com.kkrasylnykov.l20_filesandpermisionexample.fragments;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.kkrasylnykov.l20_filesandpermisionexample.R;

import java.io.File;

public class ImageFragment extends Fragment {
    public static final String KEY_PATH = "KEY_PATH";

    ImageView imageView;
    Bitmap bitmap;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_image, container, false);
        imageView = (ImageView) view.findViewById(R.id.ImageView);
        Bundle bundle = getArguments();
        if (bundle!=null){
            String strPath = bundle.getString(KEY_PATH, "");
            if(strPath.isEmpty()){
                return view;
            }

            if (bitmap==null){
                File imgFile = new  File(strPath);

                if(imgFile.exists()){
                    Display display = getActivity().getWindowManager().getDefaultDisplay();
                    Point size = new Point();
                    display.getSize(size);
                    int width = size.x;

                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inJustDecodeBounds = true;

                    BitmapFactory.decodeFile(imgFile.getAbsolutePath(), options);

                    int nOutWidth = options.outWidth;

                    int nScale = (nOutWidth/width) + 1;
                    if (nScale<1){
                        nScale = 1;
                    }
                    Log.d("devcpp","nOutWidth -> " + nOutWidth);
                    Log.d("devcpp","nScale -> " + nScale);

                    options = new BitmapFactory.Options();
                    options.inSampleSize = nScale;

                    bitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath(), options);

                    Log.d("devcpp","getWidth -> " + bitmap.getWidth());
                }
            }
            imageView.setImageBitmap(bitmap);

        }
        return view;
    }
}
