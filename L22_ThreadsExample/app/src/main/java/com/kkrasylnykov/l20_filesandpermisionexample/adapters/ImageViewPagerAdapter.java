package com.kkrasylnykov.l20_filesandpermisionexample.adapters;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.kkrasylnykov.l20_filesandpermisionexample.fragments.ImageFragment;

import java.util.ArrayList;

public class ImageViewPagerAdapter extends FragmentPagerAdapter {

    ArrayList<String> arrData;

    public ImageViewPagerAdapter(FragmentManager fm, ArrayList<String> arrData) {
        super(fm);
        this.arrData = arrData;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = new ImageFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ImageFragment.KEY_PATH, arrData.get(position));
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public int getCount() {
        return arrData.size();
    }
}
