package com.kkrasylnykov.l20_filesandpermisionexample.fragments;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.kkrasylnykov.l20_filesandpermisionexample.R;

import java.io.File;

public class ImageFragment extends Fragment {
    public static final String KEY_PATH = "KEY_PATH";

    ImageView imageView;
    Bitmap bitmap;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_image, container, false);
        imageView = (ImageView) view.findViewById(R.id.ImageView);
        Bundle bundle = getArguments();
        if (bundle!=null){
            String strPath = bundle.getString(KEY_PATH, "");
            if(strPath.isEmpty()){
                return view;
            }

            if (bitmap==null){
                File imgFile = new  File(strPath);

                if(imgFile.exists()){
                    Display display = getActivity().getWindowManager().getDefaultDisplay();
                    Point size = new Point();
                    display.getSize(size);
                    int width = size.x;

                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inJustDecodeBounds = true;

                    BitmapFactory.decodeFile(imgFile.getAbsolutePath(), options);

                    int nOutWidth = options.outWidth;

                    int nScale = (nOutWidth/width) + 1;
                    if (nScale<1){
                        nScale = 1;
                    }

                    options = new BitmapFactory.Options();
                    options.inSampleSize = nScale;

                    bitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath(), options);
                }
            }
            imageView.setImageBitmap(bitmap);

        }
        return view;
    }

    private Thread thread;

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(isVisibleToUser && bitmap!=null){
            thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    Log.d("devcpp","start");
                    int nRed = Color.rgb(255,0,0);
                    int nBlue = Color.rgb(0,0,255);
                    int nWhite = Color.rgb(255,255,255);

                    int nRedCount = 0;
                    int nWhiteCount = 0;
                    int nBlueCount = 0;
                    int z = 0;
                    while(z<20){
                        z++;
                        if (thread.isInterrupted()){
                            return;
                        }
                        for(int x=0; x<bitmap.getWidth(); x++){
                            for (int y=0; y<bitmap.getHeight(); y++){
                                int nColor = bitmap.getPixel(x,y);
                                if (nColor==nRed){
                                    nRedCount++;
                                } else if (nColor==nBlue){
                                    nBlueCount++;
                                } else if (nColor==nWhite){
                                    nWhiteCount++;
                                }
                                long ntest = 50000*50000*50000;
                                ntest += 10 + x*y*x;
                                ntest /= 10 + y*x*y;
                            }
                        }
                    }

                    try{
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getContext(), "stop", Toast.LENGTH_LONG).show();
                            }
                        });
                    } catch (IllegalStateException e){

                    }


                    Log.d("devcpp", "nRedCount -> " + nRedCount +
                            "; nBlueCount -> " + nBlueCount + "; nWhiteCount -> " + nWhiteCount);
                }

            });
            thread.start();
        } else if (!isVisibleToUser && thread!=null) {
            //thread.interrupt();
        }

    }
}
