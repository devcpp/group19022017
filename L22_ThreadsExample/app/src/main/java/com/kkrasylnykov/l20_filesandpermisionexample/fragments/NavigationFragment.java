package com.kkrasylnykov.l20_filesandpermisionexample.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kkrasylnykov.l20_filesandpermisionexample.R;

public class NavigationFragment extends Fragment {

    @Nullable
    @Override
    public synchronized View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_navigation, container, false);
        return view;
    }

    public static synchronized void test(){
        //TODO
    }
}
