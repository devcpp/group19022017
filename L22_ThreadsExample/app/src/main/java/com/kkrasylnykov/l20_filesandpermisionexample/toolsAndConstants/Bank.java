package com.kkrasylnykov.l20_filesandpermisionexample.toolsAndConstants;

public class Bank{
    private static int sCash = 100000;

    private static Bank bank;

    private Bank(){
    }

    public static Bank getInstance(){
        if (bank==null){
            bank = new Bank();
        }
        return bank;
    }

    public void addOneDollar(){
        synchronized (Bank.class){
            sCash++;
        }
    }

    public static synchronized void getOneDollar(){
        sCash--;
    }

    public static int getsCash() {
        synchronized (Bank.class){
            return sCash;
        }

    }
}