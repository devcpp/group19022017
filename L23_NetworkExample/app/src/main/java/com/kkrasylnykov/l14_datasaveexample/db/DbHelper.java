package com.kkrasylnykov.l14_datasaveexample.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.kkrasylnykov.l14_datasaveexample.toolsAndConstants.DbConstants;

public class DbHelper extends SQLiteOpenHelper {


    public DbHelper(Context context) {
        super(context, DbConstants.DB_NAME, null, DbConstants.DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        Log.d("devcpp","DbHelper -> onCreate");
        sqLiteDatabase.execSQL("CREATE TABLE " + DbConstants.DB_V2.TABLE_USER_INFO.TABLE_NAME +
                " (" + DbConstants.DB_V2.TABLE_USER_INFO.FIELD_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + DbConstants.DB_V2.TABLE_USER_INFO.FIELD_SERVER_ID + " INTEGER NOT NULL, "
                + DbConstants.DB_V2.TABLE_USER_INFO.FIELD_NAME + " TEXT NOT NULL, "
                + DbConstants.DB_V2.TABLE_USER_INFO.FIELD_SNAME + " TEXT, "
                + DbConstants.DB_V2.TABLE_USER_INFO.FIELD_EMAIL + " TEXT, "
                + DbConstants.DB_V2.TABLE_USER_INFO.FIELD_BDAY + " INTEGER);");

        sqLiteDatabase.execSQL("CREATE TABLE " + DbConstants.DB_V2.TABLE_PHONES.TABLE_NAME +
                " (" + DbConstants.DB_V2.TABLE_PHONES.FIELD_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + DbConstants.DB_V2.TABLE_PHONES.FIELD_USER_ID + " INTEGER NOT NULL, "
                + DbConstants.DB_V2.TABLE_PHONES.FIELD_PHONE + " TEXT);");
    }

    public void onCreate(SQLiteDatabase sqLiteDatabase, int nVersion) {
        if (nVersion==DbConstants.DB_VERSION){
            onCreate(sqLiteDatabase);
        } else {

        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        Log.d("devcpp","DbHelper -> onUpgrade -> " + i);
        Log.d("devcpp","DbHelper -> onUpgrade -> " + i1);
        if (i<3){
            onCreate(sqLiteDatabase, 3);
            Cursor cursor = sqLiteDatabase.query(DbConstants.DB_V1.TABLE_NAME, null, null, null, null, null, null);
            if (cursor!=null){
                if (cursor.moveToFirst()){
                    do {
                        String strName = cursor.getString(cursor.getColumnIndex(DbConstants.DB_V1.USER_INFO_FIELD_NAME));
                        String strSName = cursor.getString(cursor.getColumnIndex(DbConstants.DB_V1.USER_INFO_FIELD_SNAME));
                        String strPhone = cursor.getString(cursor.getColumnIndex(DbConstants.DB_V1.USER_INFO_FIELD_PHONE));
                        String strEmail = cursor.getString(cursor.getColumnIndex(DbConstants.DB_V1.USER_INFO_FIELD_EMAIL));
                        long nBDay = cursor.getLong(cursor.getColumnIndex(DbConstants.DB_V1.USER_INFO_FIELD_BDAY));

                        ContentValues values = new ContentValues();
                        values.put(DbConstants.DB_V2.TABLE_USER_INFO.FIELD_NAME, strName);
                        values.put(DbConstants.DB_V2.TABLE_USER_INFO.FIELD_SNAME, strSName);
                        values.put(DbConstants.DB_V2.TABLE_USER_INFO.FIELD_EMAIL, strEmail);
                        values.put(DbConstants.DB_V2.TABLE_USER_INFO.FIELD_BDAY, nBDay);
                        long nUserId = sqLiteDatabase.insert(DbConstants.DB_V2.TABLE_USER_INFO.TABLE_NAME, null, values);

                        values = new ContentValues();
                        values.put(DbConstants.DB_V2.TABLE_PHONES.FIELD_USER_ID, nUserId);
                        values.put(DbConstants.DB_V2.TABLE_PHONES.FIELD_PHONE, strPhone);
                        sqLiteDatabase.insert(DbConstants.DB_V2.TABLE_PHONES.TABLE_NAME, null, values);
                    } while (cursor.moveToNext());
                }
                cursor.close();
            }

            sqLiteDatabase.execSQL("DROP TABLE " + DbConstants.DB_V1.TABLE_NAME);
        }
    }
}
