package com.kkrasylnykov.l14_datasaveexample.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

import com.kkrasylnykov.l14_datasaveexample.toolsAndConstants.DbConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class UserInfo extends BaseEntity implements Parcelable {
    private long m_nServerId;
    private String m_strName;
    private String m_strSName;
    private String m_strEmail;
    private long m_nBDay;

    private boolean m_isExp = false;

    private ArrayList<PhoneInfo> m_arrPhones;

    public UserInfo(String m_strName, String m_strSName, String m_strEmail, long m_nBDay) {
        m_nServerId = -1;
        this.m_strName = m_strName;
        this.m_strSName = m_strSName;
        this.m_strEmail = m_strEmail;
        this.m_nBDay = m_nBDay;
        this.m_arrPhones = new ArrayList<>();
    }

    public UserInfo(Cursor cursor){
        setId(cursor.getLong(cursor.getColumnIndex(DbConstants.DB_V2.TABLE_USER_INFO.FIELD_ID)));
        this.m_nServerId = cursor.getLong(cursor.getColumnIndex(DbConstants.DB_V2.TABLE_USER_INFO.FIELD_SERVER_ID));
        this.m_strName = cursor.getString(cursor.getColumnIndex(DbConstants.DB_V2.TABLE_USER_INFO.FIELD_NAME));
        this.m_strSName = cursor.getString(cursor.getColumnIndex(DbConstants.DB_V2.TABLE_USER_INFO.FIELD_SNAME));
        this.m_strEmail = cursor.getString(cursor.getColumnIndex(DbConstants.DB_V2.TABLE_USER_INFO.FIELD_EMAIL));
        this.m_nBDay = cursor.getLong(cursor.getColumnIndex(DbConstants.DB_V2.TABLE_USER_INFO.FIELD_BDAY));
    }

    public UserInfo(JSONObject jsonObject) throws JSONException {
        setId(-1);
        this.m_nServerId = jsonObject.getLong("id");
        this.m_strName = jsonObject.getString("name");
        this.m_strSName = jsonObject.getString("sname");

        JSONArray addrJsonArray = jsonObject.getJSONArray("address");
        if (addrJsonArray.length()>0){
            this.m_strEmail = addrJsonArray.getString(0);
        } else {
            this.m_strEmail = "";
        }
        this.m_arrPhones = new ArrayList<>();
        JSONArray phonesJsonArray = jsonObject.getJSONArray("phones");
        int nCount = phonesJsonArray.length();
        for (int i=0; i<nCount; i++){
            this.m_arrPhones.add(new PhoneInfo(phonesJsonArray.getString(i)));
        }

        this.m_nBDay = 10;
    }

    public String getName() {
        return m_strName;
    }

    public void setName(String m_strName) {
        this.m_strName = m_strName;
    }

    public String getSName() {
        return m_strSName;
    }

    public void setSName(String m_strSName) {
        this.m_strSName = m_strSName;
    }

    public ArrayList<PhoneInfo> getPhones() {
        return m_arrPhones;
    }

    public void setPhones(ArrayList<PhoneInfo> m_arrPhones) {
        this.m_arrPhones = m_arrPhones;
    }

    public String getEmail() {
        return m_strEmail;
    }

    public void setEmail(String m_strEmail) {
        this.m_strEmail = m_strEmail;
    }

    public long getBDay() {
        return m_nBDay;
    }

    public void setBDay(long m_nBDay) {
        this.m_nBDay = m_nBDay;
    }

    public long getServerId() {
        return m_nServerId;
    }

    public void setServerId(long m_nServerId) {
        this.m_nServerId = m_nServerId;
    }

    public boolean isExp() {
        return m_isExp;
    }

    public void setIsExp(boolean m_isExp) {
        this.m_isExp = m_isExp;
    }

    public boolean validate(){
        boolean bResult = !"".equals(getName()) && getBDay()>0;

        return bResult;
    }

    @Override
    public ContentValues getContentValues() {
        ContentValues values = new ContentValues();
        values.put(DbConstants.DB_V2.TABLE_USER_INFO.FIELD_SERVER_ID, getServerId());
        values.put(DbConstants.DB_V2.TABLE_USER_INFO.FIELD_NAME, getName());
        values.put(DbConstants.DB_V2.TABLE_USER_INFO.FIELD_SNAME, getSName());
        values.put(DbConstants.DB_V2.TABLE_USER_INFO.FIELD_EMAIL, getEmail());
        values.put(DbConstants.DB_V2.TABLE_USER_INFO.FIELD_BDAY, getBDay());
        return values;
    }

    public JSONObject getJSON() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("name",getName());
        jsonObject.put("sname", getSName());
        if (!getEmail().isEmpty()){
            JSONArray addrJsonArray = new JSONArray();
            addrJsonArray.put(getEmail());
            jsonObject.put("address", addrJsonArray);
        }

        if (!getPhones().isEmpty()){
            JSONArray phonesJsonArray = new JSONArray();
            for (PhoneInfo phoneInfo:getPhones()){
                phonesJsonArray.put(phoneInfo.getPhone());
            }
            jsonObject.put("phones", phonesJsonArray);
        }
        return jsonObject;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(getId());
        dest.writeString(m_strName);
        dest.writeString(m_strSName);
        dest.writeString(m_strEmail);
        dest.writeLong(m_nBDay);
        dest.writeList(m_arrPhones);

    }

    public static final Parcelable.Creator<UserInfo> CREATOR
            = new Parcelable.Creator<UserInfo>() {
        public UserInfo createFromParcel(Parcel in) {
            return new UserInfo(in);
        }

        public UserInfo[] newArray(int size) {
            return new UserInfo[size];
        }
    };

    private UserInfo(Parcel in){
        setId(in.readLong());
        m_strName = in.readString();
        m_strSName = in.readString();
        m_strEmail = in.readString();
        m_nBDay = in.readLong();

        m_arrPhones = in.readArrayList(PhoneInfo.class.getClassLoader());
    }
}
