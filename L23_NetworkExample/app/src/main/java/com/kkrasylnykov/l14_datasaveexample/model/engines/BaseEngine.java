package com.kkrasylnykov.l14_datasaveexample.model.engines;

import android.app.Activity;
import android.content.Context;

public abstract class BaseEngine {
    private Activity m_context;

    public BaseEngine(Activity m_context) {
        this.m_context = m_context;
    }

    public Activity getContext() {
        return m_context;
    }

    public interface OnCallback{
        void onCallback(Object o);
    }
}
