package com.kkrasylnykov.l14_datasaveexample.model.engines;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.kkrasylnykov.l14_datasaveexample.model.PhoneInfo;
import com.kkrasylnykov.l14_datasaveexample.model.UserInfo;
import com.kkrasylnykov.l14_datasaveexample.model.wrappers.dbWrappers.PhoneInfoDBWrapper;
import com.kkrasylnykov.l14_datasaveexample.model.wrappers.dbWrappers.UserInfoDBWrapper;
import com.kkrasylnykov.l14_datasaveexample.toolsAndConstants.DbConstants;

import java.util.ArrayList;

public class PhonesInfoEngine extends BaseEngine {

    public PhonesInfoEngine(Activity m_context) {
        super(m_context);
    }

    public ArrayList<PhoneInfo> getPhonesByUserId(long nId){
        PhoneInfoDBWrapper phoneInfoDBWrapper = new PhoneInfoDBWrapper(getContext());
        return phoneInfoDBWrapper.getPhonesByUserId(nId);
    }

    public void addPhone(PhoneInfo item){
        PhoneInfoDBWrapper phoneInfoDBWrapper = new PhoneInfoDBWrapper(getContext());
        phoneInfoDBWrapper.addPhone(item);
    }

    public void updatePhone(PhoneInfo item){
        PhoneInfoDBWrapper phoneInfoDBWrapper = new PhoneInfoDBWrapper(getContext());
        phoneInfoDBWrapper.updateUser(item);
    }

    public void removePhoneById(long nId){
        PhoneInfoDBWrapper phoneInfoDBWrapper = new PhoneInfoDBWrapper(getContext());
        phoneInfoDBWrapper.removePhoneById(nId);
    }

    public void removePhonesByUserId(long nUserId){
        PhoneInfoDBWrapper phoneInfoDBWrapper = new PhoneInfoDBWrapper(getContext());
        phoneInfoDBWrapper.removePhonesByUserId(nUserId);
    }

    public void removeAll(){
        PhoneInfoDBWrapper phoneInfoDBWrapper = new PhoneInfoDBWrapper(getContext());
        phoneInfoDBWrapper.removeAll();
    }

}
