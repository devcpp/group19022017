package com.kkrasylnykov.l14_datasaveexample.model.engines;

import android.app.Activity;
import android.content.Context;

import com.kkrasylnykov.l14_datasaveexample.model.PhoneInfo;
import com.kkrasylnykov.l14_datasaveexample.model.UserInfo;
import com.kkrasylnykov.l14_datasaveexample.model.wrappers.dbWrappers.UserInfoDBWrapper;
import com.kkrasylnykov.l14_datasaveexample.model.wrappers.networkWrappers.UserInfoNetworkWrapper;

import java.util.ArrayList;

public class UserInfoEngine extends BaseEngine {

    public UserInfoEngine(Activity m_context) {
        super(m_context);
    }

    public void getAllUsers(final OnCallback onCallback){
        UserInfoDBWrapper userInfoDBWrapper = new UserInfoDBWrapper(getContext());
        ArrayList<UserInfo> arrData = userInfoDBWrapper.getAllUsers();
        if (arrData==null || arrData.isEmpty()){
            UserInfoNetworkWrapper networkWrapper = new UserInfoNetworkWrapper();
            arrData = networkWrapper.getAllUsers();
            if (!arrData.isEmpty()){
                addUsers(arrData, false);
                getAllUsers(onCallback);
                return;
            }
        } else {
            PhonesInfoEngine phonesInfoEngine = new PhonesInfoEngine(getContext());
            for(UserInfo item:arrData){
                item.setPhones(phonesInfoEngine.getPhonesByUserId(item.getId()));
            }
        }
        if (onCallback!=null){
            final Object callbacData = arrData;
            getContext().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    onCallback.onCallback(callbacData);
                }
            });

        }
    }

    public ArrayList<UserInfo> getUsersBySearchString(String strSearch){
        UserInfoDBWrapper userInfoDBWrapper = new UserInfoDBWrapper(getContext());
        ArrayList<UserInfo> arrData = userInfoDBWrapper.getUsersBySearchString(strSearch);
        PhonesInfoEngine phonesInfoEngine = new PhonesInfoEngine(getContext());
        for(UserInfo item:arrData){
            item.setPhones(phonesInfoEngine.getPhonesByUserId(item.getId()));
        }
        return arrData;
    }

    public UserInfo getUserById(long nId){
        UserInfoDBWrapper userInfoDBWrapper = new UserInfoDBWrapper(getContext());
        UserInfo userInfo = userInfoDBWrapper.getUserById(nId);
        PhonesInfoEngine phonesInfoEngine = new PhonesInfoEngine(getContext());
        userInfo.setPhones(phonesInfoEngine.getPhonesByUserId(userInfo.getId()));
        return userInfo;
    }

    public void addUsers(ArrayList<UserInfo> arrUserInfo, boolean bIsSendToServer){
        for (UserInfo userInfo:arrUserInfo){
            addUser(userInfo,bIsSendToServer);
        }

    }

    public void addUser(UserInfo userInfo){
        addUser(userInfo, true);
    }

    public void addUser(UserInfo userInfo, boolean bIsSendToServer){
        if (bIsSendToServer){
            UserInfoNetworkWrapper networkWrapper = new UserInfoNetworkWrapper();
            networkWrapper.addUser(userInfo);
        }

        UserInfoDBWrapper userInfoDBWrapper = new UserInfoDBWrapper(getContext());
        long id = userInfoDBWrapper.addUser(userInfo);
        PhonesInfoEngine phonesInfoEngine = new PhonesInfoEngine(getContext());
        for(PhoneInfo phoneInfo:userInfo.getPhones()){
            phoneInfo.setUserId(id);
            phonesInfoEngine.addPhone(phoneInfo);
        }



    }

    public void updateUser(UserInfo userInfo){
        UserInfoDBWrapper userInfoDBWrapper = new UserInfoDBWrapper(getContext());
        userInfoDBWrapper.updateUser(userInfo);
        PhonesInfoEngine phonesInfoEngine = new PhonesInfoEngine(getContext());
        for(PhoneInfo phoneInfo:userInfo.getPhones()){
            if (phoneInfo.getId()>-1){
                if (phoneInfo.getPhone().isEmpty()){
                    phonesInfoEngine.removePhoneById(phoneInfo.getId());
                } else {
                    phonesInfoEngine.updatePhone(phoneInfo);
                }

            } else {
                phoneInfo.setUserId(userInfo.getId());
                phonesInfoEngine.addPhone(phoneInfo);
            }
        }
    }

    public void removeUser(UserInfo userInfo){
        removeUserById(userInfo.getId());
    }

    public void removeUserById(long nId){
        UserInfoDBWrapper userInfoDBWrapper = new UserInfoDBWrapper(getContext());
        userInfoDBWrapper.removeUserById(nId);
        PhonesInfoEngine phonesInfoEngine = new PhonesInfoEngine(getContext());
        phonesInfoEngine.removePhonesByUserId(nId);
    }

    public void removeAll(){
        UserInfoDBWrapper userInfoDBWrapper = new UserInfoDBWrapper(getContext());
        userInfoDBWrapper.removeAll();
        PhonesInfoEngine phonesInfoEngine = new PhonesInfoEngine(getContext());
        phonesInfoEngine.removeAll();
    }
}
