package com.kkrasylnykov.l14_datasaveexample.model.wrappers.dbWrappers;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.kkrasylnykov.l14_datasaveexample.db.DbHelper;

public abstract class BaseDBWrapper {

    private DbHelper m_DbHelper;
    private String m_strTableName;

    public BaseDBWrapper(Context context, String m_strTableName) {
        this.m_DbHelper = new DbHelper(context);
        this.m_strTableName = m_strTableName;
    }

    public String getTableName() {
        return m_strTableName;
    }

    public SQLiteDatabase getWritable(){
        return m_DbHelper.getWritableDatabase();
    }

    public SQLiteDatabase getReadable(){
        return m_DbHelper.getReadableDatabase();
    }
}
