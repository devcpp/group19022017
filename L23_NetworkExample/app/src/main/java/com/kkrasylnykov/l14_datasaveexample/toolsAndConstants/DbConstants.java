package com.kkrasylnykov.l14_datasaveexample.toolsAndConstants;

public class DbConstants {
    public static final String DB_NAME = "noute_db";
    public static final int DB_VERSION = 3;

    public static class DB_V1{
        public static final String TABLE_NAME = "UserInfo";

        public static final String USER_INFO_FIELD_ID = "_id";
        public static final String USER_INFO_FIELD_NAME = "_name";
        public static final String USER_INFO_FIELD_SNAME = "_sname";
        public static final String USER_INFO_FIELD_PHONE = "_phone";
        public static final String USER_INFO_FIELD_EMAIL = "_email";
        public static final String USER_INFO_FIELD_BDAY = "_bDay";
    }

    public static class DB_V2{
        public static class TABLE_USER_INFO{
            public static final String TABLE_NAME = "UserInfoV2";
            public static final String FIELD_ID = "_id";
            public static final String FIELD_SERVER_ID = "_server_id";
            public static final String FIELD_NAME = "_name";
            public static final String FIELD_SNAME = "_sname";
            public static final String FIELD_EMAIL = "_email";
            public static final String FIELD_BDAY = "_bDay";
        }

        public static class TABLE_PHONES{
            public static final String TABLE_NAME = "PhonesInfo";
            public static final String FIELD_ID = "_id";
            public static final String FIELD_USER_ID = "_user_id";
            public static final String FIELD_PHONE = "_phone";
        }
    }

}
