package com.kkrasylnykov.l24_serviceexample.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.kkrasylnykov.l24_serviceexample.R;
import com.kkrasylnykov.l24_serviceexample.services.LoadService;

public class LoadActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String NAME = "test123";

    TextView progressTextView;
    Button loadButton;

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent!=null){
                Bundle bundle = intent.getExtras();
                if(bundle!=null){
                    int nProc = bundle.getInt(LoadService.KEY_PROC, 0);
                    progressTextView.setText("Current progress: " + nProc);
                }
            }

        }
    };

    @Override
    protected void onStart() {
        super.onStart();
        registerReceiver(broadcastReceiver, new IntentFilter(LoadService.ACTION+NAME));
    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
            unregisterReceiver(broadcastReceiver);
        } catch (Exception e){

        }

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_load);

        loadButton = (Button) findViewById(R.id.btnStartLoad);
        loadButton.setOnClickListener(this);
        progressTextView = (TextView) findViewById(R.id.prograssLoadTextView);
    }


    @Override
    public void onClick(View v) {
        Intent intent =new Intent(this, LoadService.class);
        intent.putExtra(LoadService.KEY_URL, "http://p.xutpuk.pp.ua/test.zip");
        intent.putExtra(LoadService.KEY_NAME, NAME);
        startService(intent);
    }
}
