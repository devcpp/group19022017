package com.kkrasylnykov.l24_serviceexample.services;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.support.annotation.IntDef;
import android.support.annotation.Nullable;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;

public class LoadService extends Service {
    public static final String ACTION = "com.kkrasylnykov.l24_serviceexample.services.LoadService.";

    public static final String KEY_URL = "KEY_URL";
    public static final String KEY_NAME = "KEY_NAME";

    public static final String KEY_PROC = "KEY_PROC";

    private static final int BUFFER_SIZE = 4096;

    private Thread thread;
    private HashMap<String, String> data = new HashMap();

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("devcpp", "onCreate");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("devcpp", "onStartCommand");
        String strUrl = "";
        String strName = "";
        if (intent!=null){
            Bundle bundle = intent.getExtras();
            if(bundle!=null){
                strUrl = bundle.getString(KEY_URL, "");
                strName = bundle.getString(KEY_NAME, "");
            }
        }
        Log.d("devcpp", "onStartCommand -> KEY_URL -> " + strUrl);
        Log.d("devcpp", "onStartCommand -> KEY_NAME -> " + strName);

        if (thread!=null){
            data.put(strName, strUrl);
        } else {
            thread = new Thread(new LoadFile(strName, strUrl));
            thread.start();
        }
        //stopSelf();
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        Log.d("devcpp", "onDestroy");
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public class LoadFile implements Runnable{
        String strName;
        String strPath;

        public LoadFile(String strName, String strPath) {
            this.strName = strName;
            this.strPath = strPath;
        }

        @Override
        public void run() {
            try {
                URL url = url = new URL(strPath);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setReadTimeout(10000);
                connection.setConnectTimeout(10000);
                connection.setRequestMethod("GET");

                connection.connect();

                InputStream inputStream = connection.getInputStream();
                String saveFilePath = new File(LoadService.this.getFilesDir(), strName).getAbsolutePath();
                FileOutputStream outputStream = new FileOutputStream(saveFilePath);
                int contentLength = connection.getContentLength();
                int counDownloadByte = 0;
                int bytesRead = -1;
                byte[] buffer = new byte[BUFFER_SIZE];

                int old_proc = 0;

                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    outputStream.write(buffer, 0, bytesRead);
                    counDownloadByte += bytesRead;

                    int proc = (int)((((float)counDownloadByte)/contentLength)*100);
                    if (old_proc<proc){
                        old_proc = proc;
                        Intent intent = new Intent();
                        intent.setAction(ACTION+strName);
                        intent.putExtra(KEY_PROC, proc);
                        LoadService.this.sendBroadcast(intent);
                    }
                }

                outputStream.close();
                inputStream.close();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }


        }
    }
}
