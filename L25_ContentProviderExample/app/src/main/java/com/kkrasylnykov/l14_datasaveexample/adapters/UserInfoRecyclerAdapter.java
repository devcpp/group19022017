package com.kkrasylnykov.l14_datasaveexample.adapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.kkrasylnykov.l14_datasaveexample.R;
import com.kkrasylnykov.l14_datasaveexample.model.BaseEntity;
import com.kkrasylnykov.l14_datasaveexample.model.PhoneInfo;
import com.kkrasylnykov.l14_datasaveexample.model.UserInfo;

import java.util.ArrayList;

public class UserInfoRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int USER_INFO = 1011;
    private static final int PHONE_INFO = 10110;
    private static final int ADDR_INFO = 10111;
    private static final int TOP_ITEM = 1012;
    private static final int BOTTOM_ITEM = 1013;

    private ArrayList<UserInfo> m_arrUsers;
    private ArrayList<BaseEntity> m_arrShowItems;
    private OnClickUserInfoItem m_OnClickUserInfoItem = null;

    public UserInfoRecyclerAdapter(ArrayList<UserInfo> arrUsers){
        m_arrUsers = arrUsers;
        m_arrShowItems = new ArrayList<>();
        m_arrShowItems.addAll(arrUsers);
    }

    public void setOnClickUserInfoItem(OnClickUserInfoItem onClickUserInfoItem){
        m_OnClickUserInfoItem = onClickUserInfoItem;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        switch (viewType){
            case USER_INFO:
                View viewUserInfo  = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_user_info, parent, false);
                viewHolder = new UserInfoViewHolder(viewUserInfo);
                break;
            case PHONE_INFO:
                View viewPhoneInfo  = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_phone_info, parent, false);
                viewHolder = new PhoneInfoViewHolder(viewPhoneInfo);
                break;
            case TOP_ITEM:
                View topItemUserInfo  = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_top, parent, false);
                viewHolder = new TopItemViewHolder(topItemUserInfo);
                break;
            case BOTTOM_ITEM:
                View buttomItemUserInfo  = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_bottom, parent, false);
                viewHolder = new BottomItemViewHolder(buttomItemUserInfo);
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (getItemViewType(position)){
            case USER_INFO:
                UserInfoViewHolder userInfoViewHolder = (UserInfoViewHolder) holder;
                final UserInfo userInfo = (UserInfo) m_arrShowItems.get(position-1);
                userInfoViewHolder.fullnameTextView.setText(userInfo.getName() + " " + userInfo.getSName());
//                String strPhone="";
//                for (PhoneInfo phoneInfo:userInfo.getPhones()){
//                    if (!strPhone.isEmpty()){
//                        strPhone += "\n";
//                    }
//                    strPhone += phoneInfo.getPhone();
//                }
//                userInfoViewHolder.phoneTextView.setText(strPhone);
//                userInfoViewHolder.emailTextView.setText(userInfo.getEmail());
                userInfoViewHolder.editButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(m_OnClickUserInfoItem!=null){
                            m_OnClickUserInfoItem.onClickUserInfoItem(userInfo);
                        }
                    }
                });
                userInfoViewHolder.rootView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ArrayList<PhoneInfo> arrPhone = userInfo.getPhones();
                        int nPosition = m_arrShowItems.indexOf(userInfo) + 1;
                        if (userInfo.isExp()){
                            for (int n=nPosition; n<nPosition+arrPhone.size(); n++){
                                m_arrShowItems.remove(nPosition);
                            }
//                            for (int n=nPosition+1; n<(nPosition+1)+arrPhone.size(); n++){
//                                notifyItemRemoved(nPosition+1);
//                                Log.d("devcpp"," n-> " + n);
//                            }
                            notifyDataSetChanged();

                        } else {
                            m_arrShowItems.addAll(nPosition++, arrPhone);
                            for (int n=nPosition; n<nPosition+arrPhone.size(); n++){
                                notifyItemInserted(n);
                            }
                        }
                        userInfo.setIsExp(!userInfo.isExp());
                    }
                });
                break;
            case PHONE_INFO:
                PhoneInfoViewHolder phoneInfoViewHolder = (PhoneInfoViewHolder) holder;
                PhoneInfo phoneInfo = (PhoneInfo) m_arrShowItems.get(position-1);
                phoneInfoViewHolder.phoneTextView.setText(phoneInfo.getPhone());
                break;
            case TOP_ITEM:
                TopItemViewHolder topItemViewHolder = (TopItemViewHolder) holder;
                topItemViewHolder.titleTextView.setText("Items count " + m_arrUsers.size());
                break;
            case BOTTOM_ITEM:
                BottomItemViewHolder bottomItemViewHolder = (BottomItemViewHolder) holder;
                bottomItemViewHolder.imageView.setImageResource(R.mipmap.ic_launcher);
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        int nType = USER_INFO;
        if (position==0){
            nType = TOP_ITEM;
        } else if(position==getItemCount()-1){
            nType = BOTTOM_ITEM;
        } else if (m_arrShowItems.get(position-1) instanceof PhoneInfo){
            nType = PHONE_INFO;
        }
        return nType;
    }

    public void updateData(ArrayList<UserInfo> arrData){
        m_arrUsers.clear();
        m_arrUsers.addAll(arrData);
        m_arrShowItems.clear();
        m_arrShowItems.addAll(arrData);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return m_arrShowItems.size() + 2;
    }

    public class TopItemViewHolder extends RecyclerView.ViewHolder{
        View rootView;
        TextView titleTextView;

        public TopItemViewHolder(View itemView) {
            super(itemView);
            rootView = itemView;
            titleTextView = (TextView) rootView.findViewById(R.id.titleTextViewTopItem);
        }
    }

    public class BottomItemViewHolder extends RecyclerView.ViewHolder{
        View rootView;
        ImageView imageView;

        public BottomItemViewHolder(View itemView) {
            super(itemView);
            rootView = itemView;
            imageView = (ImageView) rootView.findViewById(R.id.imageViewBottomItem);
        }
    }

    public class UserInfoViewHolder extends RecyclerView.ViewHolder{
        View rootView;
        TextView fullnameTextView;
        Button editButton;

        public UserInfoViewHolder(View itemView) {
            super(itemView);
            rootView = itemView;
            fullnameTextView = (TextView) rootView.findViewById(R.id.fullnameTextViewItem);
            editButton = (Button) rootView.findViewById(R.id.editButtonItem);
        }
    }

    public class PhoneInfoViewHolder extends RecyclerView.ViewHolder{
        View rootView;
        TextView phoneTextView;

        public PhoneInfoViewHolder(View itemView) {
            super(itemView);
            rootView = itemView;
            phoneTextView = (TextView) rootView.findViewById(R.id.phoneTextViewItem);

        }
    }

    public interface OnClickUserInfoItem{
        void onClickUserInfoItem(UserInfo userInfo);
    }
}
