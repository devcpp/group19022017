package com.kkrasylnykov.l14_datasaveexample.model.wrappers.dbWrappers;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import com.kkrasylnykov.l14_datasaveexample.db.DbHelper;

public abstract class BaseDBWrapper {

    private Context m_Context;
    private Uri m_uriTable;

    public BaseDBWrapper(Context context, Uri uriTable) {
        this.m_Context = context;
        this.m_uriTable = uriTable;
    }

    public Context getContext() {
        return m_Context;
    }

    public Uri getUriTable() {
        return m_uriTable;
    }
}
