package com.kkrasylnykov.l14_datasaveexample.model.wrappers.dbWrappers;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import com.kkrasylnykov.l14_datasaveexample.model.PhoneInfo;
import com.kkrasylnykov.l14_datasaveexample.model.UserInfo;
import com.kkrasylnykov.l14_datasaveexample.providers.DbContentProvider;
import com.kkrasylnykov.l14_datasaveexample.toolsAndConstants.DbConstants;

import java.util.ArrayList;

public class PhoneInfoDBWrapper extends BaseDBWrapper {

    public PhoneInfoDBWrapper(Context context) {
        super(context, DbContentProvider.PHONE_INFO_URI);
    }

    public ArrayList<PhoneInfo> getPhonesByUserId(long nId){
        ArrayList<PhoneInfo> arrResult = new ArrayList<>();
        String strRequest = DbConstants.DB_V2.TABLE_PHONES.FIELD_USER_ID + "=?";
        String arrArgs[] = new String[]{Long.toString(nId)};
        Cursor cursor = getContext().getContentResolver().query(getUriTable(),null,strRequest,arrArgs,null);
        try{
            if (cursor!=null && cursor.moveToFirst()){
                do{
                    arrResult.add(new PhoneInfo(cursor));
                }while (cursor.moveToNext());
            }
        } finally {
            if (cursor!=null){
                cursor.close();
            }
        }
        return arrResult;
    }

    public void addPhone(PhoneInfo item){
        getContext().getContentResolver().insert(getUriTable(), item.getContentValues());
    }

    public void updateUser(PhoneInfo item){
        String strRequest = DbConstants.DB_V2.TABLE_PHONES.FIELD_ID + "=?";
        String arrArgs[] = new String[]{Long.toString(item.getId())};
        getContext().getContentResolver().update(getUriTable(), item.getContentValues(), strRequest, arrArgs);
    }

    public void removePhoneById(long nId){
        String strRequest = DbConstants.DB_V2.TABLE_PHONES.FIELD_ID + "=?";
        String arrArgs[] = new String[]{Long.toString(nId)};
        getContext().getContentResolver().delete(getUriTable(),strRequest,arrArgs);
    }

    public void removePhonesByUserId(long nUserId){
        String strRequest = DbConstants.DB_V2.TABLE_PHONES.FIELD_USER_ID + "=?";
        String arrArgs[] = new String[]{Long.toString(nUserId)};
        getContext().getContentResolver().delete(getUriTable(),strRequest,arrArgs);
    }

    public void removeAll(){
        getContext().getContentResolver().delete(getUriTable(),null,null);
    }
}
