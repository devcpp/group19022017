package com.kkrasylnykov.l14_datasaveexample.model.wrappers.dbWrappers;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kkrasylnykov.l14_datasaveexample.db.DbHelper;
import com.kkrasylnykov.l14_datasaveexample.model.UserInfo;
import com.kkrasylnykov.l14_datasaveexample.providers.DbContentProvider;
import com.kkrasylnykov.l14_datasaveexample.toolsAndConstants.DbConstants;

import java.util.ArrayList;

public class UserInfoDBWrapper extends BaseDBWrapper {

    public UserInfoDBWrapper(Context context) {
        super(context, DbContentProvider.USER_INFO_URI);
    }

    public ArrayList<UserInfo> getAllUsers(){
        ArrayList<UserInfo> arrResult = new ArrayList<>();
        Cursor cursor = getContext().getContentResolver().query(getUriTable(),null,null,null,null);
        try{
            if (cursor!=null && cursor.moveToFirst()){
                do{
                    UserInfo userInfo = new UserInfo(cursor);
                    arrResult.add(userInfo);
                } while (cursor.moveToNext());

            }
        } finally {
            if (cursor!=null){
                cursor.close();
            }
        }
        return arrResult;
    }

    public ArrayList<UserInfo> getUsersBySearchString(String strSearch){
        strSearch = "%" + strSearch + "%";
        ArrayList<UserInfo> arrResult = new ArrayList<>();
//        SQLiteDatabase db = getReadable();
//        String strRequest = DbConstants.USER_INFO_FIELD_NAME + " LIKE ? OR "
//                + DbConstants.USER_INFO_FIELD_SNAME + " LIKE ? OR "
//                + DbConstants.USER_INFO_FIELD_PHONE + " LIKE ? OR "
//                + DbConstants.USER_INFO_FIELD_EMAIL + " LIKE ? ";
//        String arrArgs[] = new String[]{strSearch, strSearch, strSearch, strSearch};
//        Cursor cursor = db.query(getTableName(),null,strRequest,arrArgs,null,null,null);
//        try{
//            if (cursor!=null && cursor.moveToFirst()){
//                do{
//                    UserInfo userInfo = new UserInfo(cursor);
//                    arrResult.add(userInfo);
//                } while (cursor.moveToNext());
//
//            }
//        } finally {
//            if (cursor!=null){
//                cursor.close();
//            }
//            db.close();
//        }
        return arrResult;
    }

    public UserInfo getUserById(long nId){
        UserInfo userInfo = null;
        String strRequest = DbConstants.DB_V2.TABLE_USER_INFO.FIELD_ID + "=?";
        String arrArgs[] = new String[]{Long.toString(nId)};
        Cursor cursor = getContext().getContentResolver().query(getUriTable(),null,strRequest,arrArgs,null);
        try{
            if (cursor!=null && cursor.moveToFirst()){
                userInfo = new UserInfo(cursor);
            }
        } finally {
            if (cursor!=null){
                cursor.close();
            }
        }
        return userInfo;
    }

    public long addUser(UserInfo userInfo){
        //TODO Add Implementation return ID
        getContext().getContentResolver().insert(getUriTable(), userInfo.getContentValues());
        long id = 0;
        return id;
    }

    public void updateUser(UserInfo userInfo){
        String strRequest = DbConstants.DB_V2.TABLE_USER_INFO.FIELD_ID + "=?";
        String arrArgs[] = new String[]{Long.toString(userInfo.getId())};
        getContext().getContentResolver().update(getUriTable(), userInfo.getContentValues(), strRequest, arrArgs);
    }

    public void removeUserById(long nId){
        String strRequest = DbConstants.DB_V2.TABLE_USER_INFO.FIELD_ID + "=?";
        String arrArgs[] = new String[]{Long.toString(nId)};
        getContext().getContentResolver().delete(getUriTable(),strRequest,arrArgs);
    }

    public void removeAll(){
        getContext().getContentResolver().delete(getUriTable(),null,null);
    }
}
