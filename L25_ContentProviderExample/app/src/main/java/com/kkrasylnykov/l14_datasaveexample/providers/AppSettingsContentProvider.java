package com.kkrasylnykov.l14_datasaveexample.providers;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.SharedPreferences;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.kkrasylnykov.l14_datasaveexample.toolsAndConstants.AppSettings;
import com.kkrasylnykov.l14_datasaveexample.toolsAndConstants.DbConstants;

public class AppSettingsContentProvider extends ContentProvider {

    public static final String KEY_DATA = "KEY_DATA";

    private static final String AUTHORITY = "com.kkrasylnykov.l14_datasaveexample.providers.AppSettingsContentProvider.jC9MaHGkTy";

    private static final String KEY_BOOLEAN_IS_SHOW_TERMS = "KEY_BOOLEAN_IS_SHOW_TERMS";
    private static final String KEY_BOOLEAN_IS_EDIT_ACTIVITY_FINISH = "KEY_BOOLEAN_IS_EDIT_ACTIVITY_FINISH";
    private static final String KEY_LONG_DATE_LAST_UPDATE = "KEY_LONG_DATE_LAST_UPDATE";
    private static final String KEY_LONG_DATE_LAST_START = "KEY_LONG_DATE_LAST_START";

    private static final String SHOW_TERMS           = KEY_BOOLEAN_IS_SHOW_TERMS.toLowerCase();
    private static final String EDIT_ACTIVITY_FINISH = KEY_BOOLEAN_IS_EDIT_ACTIVITY_FINISH.toLowerCase();
    private static final String DATE_LAST_UPDATE     = KEY_LONG_DATE_LAST_UPDATE.toLowerCase();
    private static final String DATE_LAST_START      = KEY_LONG_DATE_LAST_START.toLowerCase();

    public static final Uri SHOW_TERMS_URI              = Uri.parse("content://" + AUTHORITY + "/" + SHOW_TERMS);
    public static final Uri EDIT_ACTIVITY_FINISH_URI    = Uri.parse("content://" + AUTHORITY + "/" + EDIT_ACTIVITY_FINISH);
    public static final Uri DATE_LAST_UPDATE_URI        = Uri.parse("content://" + AUTHORITY + "/" + DATE_LAST_UPDATE);
    public static final Uri DATE_LAST_START_URI         = Uri.parse("content://" + AUTHORITY + "/" + DATE_LAST_START);

    private static final int URI_SHOW_TERMS             = 1;
    private static final int URI_EDIT_ACTIVITY_FINISH   = 2;
    private static final int URI_DATE_LAST_UPDATE       = 3;
    private static final int URI_DATE_LAST_START        = 4;

    private static final UriMatcher uriMatcher;
    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(AUTHORITY, SHOW_TERMS, URI_SHOW_TERMS);
        uriMatcher.addURI(AUTHORITY, EDIT_ACTIVITY_FINISH, URI_EDIT_ACTIVITY_FINISH);
        uriMatcher.addURI(AUTHORITY, DATE_LAST_UPDATE, URI_DATE_LAST_UPDATE);
        uriMatcher.addURI(AUTHORITY, DATE_LAST_START, URI_DATE_LAST_START);
    }

    private SharedPreferences m_SharedPreferences;

    @Override
    public boolean onCreate() {
        m_SharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        return m_SharedPreferences!=null;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        Object o = null;
        switch (uriMatcher.match(uri)){
            case URI_SHOW_TERMS:
                o = m_SharedPreferences.getBoolean(KEY_BOOLEAN_IS_SHOW_TERMS, true)?1:0;
                break;
            case URI_EDIT_ACTIVITY_FINISH:
                o = m_SharedPreferences.getBoolean(KEY_BOOLEAN_IS_EDIT_ACTIVITY_FINISH, false)?1:0;
                break;
            case URI_DATE_LAST_UPDATE:
                o = m_SharedPreferences.getLong(KEY_LONG_DATE_LAST_UPDATE, -1);
                break;
            case URI_DATE_LAST_START:
                o = m_SharedPreferences.getLong(KEY_LONG_DATE_LAST_START, -1);
                break;
        }

        MatrixCursor cursor = new MatrixCursor(new String[]{KEY_DATA}, 1);
        Object[] arrValue = {o};
        cursor.addRow(arrValue);
        return cursor;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        SharedPreferences.Editor editor = m_SharedPreferences.edit();
        switch (uriMatcher.match(uri)){
            case URI_SHOW_TERMS:
                boolean bShowTerms = values.getAsBoolean(KEY_DATA);
                editor.putBoolean(KEY_BOOLEAN_IS_SHOW_TERMS, bShowTerms);
                break;
            case URI_EDIT_ACTIVITY_FINISH:
                boolean bIsEditFinish = values.getAsBoolean(KEY_DATA);
                editor.putBoolean(KEY_BOOLEAN_IS_EDIT_ACTIVITY_FINISH, bIsEditFinish);
                break;
            case URI_DATE_LAST_UPDATE:
                long nLastUpdate = values.getAsLong(KEY_DATA);
                editor.putLong(KEY_LONG_DATE_LAST_UPDATE, nLastUpdate);
                break;
            case URI_DATE_LAST_START:
                long nLastStart = values.getAsLong(KEY_DATA);
                editor.putLong(KEY_LONG_DATE_LAST_START, nLastStart);
                break;
        }
        editor.commit();
        return null;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }
}
