package com.kkrasylnykov.l14_datasaveexample.providers;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.kkrasylnykov.l14_datasaveexample.db.DbHelper;
import com.kkrasylnykov.l14_datasaveexample.toolsAndConstants.DbConstants;


public class DbContentProvider extends ContentProvider {

    private static final String AUTHORITY = "com.kkrasylnykov.l14_datasaveexample.providers.DbContentProvider.zGGk_6.ubv";

    private static final String TABLE_USER_INFO     = DbConstants.DB_V2.TABLE_USER_INFO.TABLE_NAME.toLowerCase();
    private static final String TABLE_PHONE_INFO     = DbConstants.DB_V2.TABLE_PHONES.TABLE_NAME.toLowerCase();

    public static final Uri USER_INFO_URI   = Uri.parse("content://" + AUTHORITY + "/" + TABLE_USER_INFO);
    public static final Uri PHONE_INFO_URI  = Uri.parse("content://" + AUTHORITY + "/" + TABLE_PHONE_INFO);

    private static final int URI_USER_INFO       = 1;
    private static final int URI_PHONE_INFO      = 2;

    private static final UriMatcher uriMatcher;
    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(AUTHORITY, TABLE_USER_INFO, URI_USER_INFO);
        uriMatcher.addURI(AUTHORITY, TABLE_PHONE_INFO, URI_PHONE_INFO);
    }

    private DbHelper dbHelper;

    @Override
    public boolean onCreate() {
        dbHelper = new DbHelper(getContext());

        return dbHelper!=null;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection,
                        @Nullable String selection, @Nullable String[] selectionArgs,
                        @Nullable String sortOrder) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String strTableName = getType(uri);
        if (strTableName.isEmpty()){
            return null;
        }

        return db.query(strTableName, projection, selection, selectionArgs, null, null, sortOrder);
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        String strTableName = getType(uri);
        if (strTableName.isEmpty()){
            return null;
        }

        db.insert(strTableName, null, values);
        return null;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        String strTableName = getType(uri);
        if (strTableName.isEmpty()){
            return -1;
        }


        return db.delete(strTableName, selection, selectionArgs);
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        String strTableName = getType(uri);
        if (strTableName.isEmpty()){
            return -1;
        }


        return db.update(strTableName, values, selection, selectionArgs);
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        String strResult = "";
        switch (uriMatcher.match(uri)){
            case URI_USER_INFO:
                strResult = DbConstants.DB_V2.TABLE_USER_INFO.TABLE_NAME;
                break;
            case URI_PHONE_INFO:
                strResult = DbConstants.DB_V2.TABLE_PHONES.TABLE_NAME;
                break;
        }
        return strResult;
    }
}
