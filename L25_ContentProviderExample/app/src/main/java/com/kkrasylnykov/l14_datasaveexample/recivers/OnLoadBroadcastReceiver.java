package com.kkrasylnykov.l14_datasaveexample.recivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.kkrasylnykov.l14_datasaveexample.services.UpdateService;

public class OnLoadBroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("devcpp","onReceive -> ");
        Intent updateIntent = new Intent(context, UpdateService.class);
        context.startService(updateIntent);
    }
}
