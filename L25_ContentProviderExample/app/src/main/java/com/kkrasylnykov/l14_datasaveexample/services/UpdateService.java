package com.kkrasylnykov.l14_datasaveexample.services;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.util.Log;

import com.kkrasylnykov.l14_datasaveexample.model.UserInfo;
import com.kkrasylnykov.l14_datasaveexample.model.engines.BaseEngine;
import com.kkrasylnykov.l14_datasaveexample.model.engines.UserInfoEngine;
import com.kkrasylnykov.l14_datasaveexample.toolsAndConstants.AppSettings;

import java.util.ArrayList;

public class UpdateService extends IntentService {
    public UpdateService() {
        this("UpdateService");
    }

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public UpdateService(String name) {

        super(name);
        Log.d("devcpp","UpdateService -> " + name);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        Log.d("devcpp","onHandleIntent -> ");
        final UserInfoEngine userInfoEngine = new UserInfoEngine(UpdateService.this);
        while (true){
            Log.d("devcpp","startUpdate -> ");
            userInfoEngine.getAllNetworkUsers(new BaseEngine.OnCallback() {
                @Override
                public void onCallback(Object o) {
                    ArrayList<UserInfo> arrData = (ArrayList<UserInfo>) o;
                    AppSettings appSettings = new AppSettings(UpdateService.this);
                    Log.d("devcpp","startUpdate -> " + appSettings.getLastStart());
                    long nTimeLastUpdate = appSettings.getLastUpdate();
                    Log.d("devcpp","nTimeLastUpdate -> " + nTimeLastUpdate);
                    for (UserInfo info : arrData){
                        if (info.getUpdateTime()>nTimeLastUpdate){
                            userInfoEngine.addUser(info, false);
                            Log.d("devcpp","info -> " + info.toString());
                            long currentTime= System.currentTimeMillis();
                            appSettings.setLastUpdate(currentTime);
                        }
                    }
                }
            });

            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
