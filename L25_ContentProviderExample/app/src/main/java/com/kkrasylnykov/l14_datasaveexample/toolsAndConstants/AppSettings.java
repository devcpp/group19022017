package com.kkrasylnykov.l14_datasaveexample.toolsAndConstants;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.preference.PreferenceManager;
import android.util.Log;

import com.kkrasylnykov.l14_datasaveexample.providers.AppSettingsContentProvider;

public class AppSettings {

    Context context;



    public AppSettings(Context context){
        this.context = context;
    }

    public boolean isShowTerms(){
        boolean bResult = false;
        Cursor cursor = context.getContentResolver().query(AppSettingsContentProvider.SHOW_TERMS_URI, null,null,null,null);
        if (cursor!=null){
            if(cursor.moveToFirst()){
                bResult = cursor.getInt(0)==1;
            }
            cursor.close();
        }
        return bResult;
    }

    public void setShowTerms(boolean bIsShow){
        ContentValues values = new ContentValues();
        values.put(AppSettingsContentProvider.KEY_DATA, bIsShow);
        context.getContentResolver().insert(AppSettingsContentProvider.SHOW_TERMS_URI, values);
    }

    public boolean isFinishEditActivity(){
        boolean bResult = false;
        Cursor cursor = context.getContentResolver().query(AppSettingsContentProvider.EDIT_ACTIVITY_FINISH_URI, null,null,null,null);
        if (cursor!=null){
            if(cursor.moveToFirst()){
                bResult = cursor.getInt(0)==1;
            }
            cursor.close();
        }
        return bResult;
    }

    public void setIsFinishEditActivity(boolean bIsFinish){
        ContentValues values = new ContentValues();
        values.put(AppSettingsContentProvider.KEY_DATA, bIsFinish);
        context.getContentResolver().insert(AppSettingsContentProvider.EDIT_ACTIVITY_FINISH_URI, values);
    }

    public long getLastUpdate(){
        long nResult = -1;
        Cursor cursor = context.getContentResolver().query(AppSettingsContentProvider.DATE_LAST_UPDATE_URI, null,null,null,null);
        if (cursor!=null){
            if(cursor.moveToFirst()){
                nResult = cursor.getLong(0);
            }
            cursor.close();
        }
        return nResult;
    }

    public void setLastUpdate(long nLastUpdate){
        nLastUpdate = nLastUpdate/1000;
        ContentValues values = new ContentValues();
        values.put(AppSettingsContentProvider.KEY_DATA, nLastUpdate);
        context.getContentResolver().insert(AppSettingsContentProvider.DATE_LAST_UPDATE_URI, values);
    }

    public long getLastStart(){
        long nResult = -1;
        Cursor cursor = context.getContentResolver().query(AppSettingsContentProvider.DATE_LAST_START_URI, null,null,null,null);
        if (cursor!=null){
            if(cursor.moveToFirst()){
                nResult = cursor.getLong(0);
            }
            cursor.close();
        }
        return nResult;
    }

    public void setLastStart(long nLastStart){
        nLastStart = nLastStart/1000;
        Log.d("devcpp","nLastStart -> " + nLastStart);
        ContentValues values = new ContentValues();
        values.put(AppSettingsContentProvider.KEY_DATA, nLastStart);
        context.getContentResolver().insert(AppSettingsContentProvider.DATE_LAST_START_URI, values);
    }

}
