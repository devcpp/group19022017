package com.kkrasylnykov.l26_externalcontentprovider;

import android.database.Cursor;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends AppCompatActivity {
    private static final String AUTHORITY = "com.kkrasylnykov.l14_datasaveexample.providers.DbContentProvider.zGGk_6.ubv";

    private static final String TABLE_USER_INFO     = "UserInfoV2".toLowerCase();
    private static final String TABLE_PHONE_INFO     = "PhonesInfo".toLowerCase();

    public static final Uri USER_INFO_URI   = Uri.parse("content://" + AUTHORITY + "/" + TABLE_USER_INFO);
    public static final Uri PHONE_INFO_URI  = Uri.parse("content://" + AUTHORITY + "/" + TABLE_PHONE_INFO);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Cursor cursor = getContentResolver().query(USER_INFO_URI, null, null, null, null);

        if (cursor!=null){
            if(cursor.moveToFirst()){
                do{
                    long nId = cursor.getLong(0);
                    long nServerId = cursor.getLong(1);
                    String strName = cursor.getString(2);
                    String strSName = cursor.getString(3);
                    Log.d("devcpp", nId + " " + nServerId + " " + strName + " " + strSName);
                } while (cursor.moveToNext());
            }
            cursor.close();
        }
    }
}
