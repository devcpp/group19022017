package com.kkrasylnykov.l27_dialogsexample;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.os.PersistableBundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.AlertDialogButton).setOnClickListener(this);
        findViewById(R.id.ProgressDialogButton).setOnClickListener(this);
        findViewById(R.id.TimeDialogButton).setOnClickListener(this);
        findViewById(R.id.DateDialogButton).setOnClickListener(this);
        findViewById(R.id.CustomDialogButtom).setOnClickListener(this);


        if (savedInstanceState!=null){
            m_nCurrentDialog = savedInstanceState.getInt(KEY_CURRENT_DIALOG, -1);
            Log.d("devcpp","savedInstanceState -> " + m_nCurrentDialog);
            showCurrentDialog();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("devcpp","onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("devcpp","onResume");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("devcpp","onRestart");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("devcpp","onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("devcpp","onStop");
    }

    private void showCurrentDialog(){
        if (m_nCurrentDialog==-1){
            return;
        }
        final Calendar calendar = Calendar.getInstance();
        switch (m_nCurrentDialog){
            case R.id.AlertDialogButton:
                AlertDialog.Builder alerDialogBuilder = new AlertDialog.Builder(this);
                alerDialogBuilder.setMessage("Hello!");
                alerDialogBuilder.setTitle("Test");
                alerDialogBuilder.setIcon(R.drawable.ic_error_outline_black_24dp);
                alerDialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(MainActivity.this, "Test Ok", Toast.LENGTH_SHORT).show();
                        m_nCurrentDialog = -1;
                    }
                });
                alerDialogBuilder.setNegativeButton("Negative", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(MainActivity.this, "Test Negative", Toast.LENGTH_SHORT).show();
                        m_nCurrentDialog = -1;
                    }
                });
                alerDialogBuilder.setNeutralButton("Neutral", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(MainActivity.this, "Test Neutral", Toast.LENGTH_SHORT).show();
                        m_nCurrentDialog = -1;
                    }
                });

                alerDialogBuilder.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        Toast.makeText(MainActivity.this, "Test onCancel", Toast.LENGTH_SHORT).show();
                        m_nCurrentDialog = -1;
                    }
                });

                alerDialogBuilder.setCancelable(false);

                AlertDialog alertDialog = alerDialogBuilder.create();
                showDialog(alertDialog);
                //alertDialog.show();
                break;
            case R.id.ProgressDialogButton:
                final ProgressDialog progressDialog = new ProgressDialog(this);
                progressDialog.setMessage("Подождите...");
                progressDialog.setCancelable(false);
                showDialog(progressDialog);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Thread.sleep(2500);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                hideDialog();
                            }
                        });
                    }
                }).start();

                break;
            case R.id.TimeDialogButton:
                TimePickerDialog.OnTimeSetListener onTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        Toast.makeText(MainActivity.this, "Test time " + hourOfDay + ":" + minute, Toast.LENGTH_SHORT).show();
                        m_nCurrentDialog = -1;
                    }
                };
                TimePickerDialog timePickerDialog = new TimePickerDialog(this,onTimeSetListener, calendar.get(Calendar.HOUR), calendar.get(Calendar.MINUTE), is24);
                is24 = !is24;
                showDialog(timePickerDialog);
                break;
            case R.id.DateDialogButton:
                DatePickerDialog.OnDateSetListener onDateSetListener = new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        calendar.set(year, month, dayOfMonth);
                        SimpleDateFormat format = new SimpleDateFormat("dd / MM / yyyy");
                        String strDate = format.format(calendar.getTime());
                        Toast.makeText(MainActivity.this, "Test date " + strDate, Toast.LENGTH_SHORT).show();
                        m_nCurrentDialog = -1;
                    }
                };
                DatePickerDialog datePickerDialog = new DatePickerDialog(this, onDateSetListener, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),calendar.get(Calendar.DAY_OF_MONTH));
                showDialog(datePickerDialog);
                break;
            case R.id.CustomDialogButtom:
                m_nCurrentDialog = -1;
                CustomDialog customDialog = new CustomDialog();
                customDialog.show(getFragmentManager(), "customDialog");
                break;
        }
    }

    @Override
    public void onClick(View v) {
        m_nCurrentDialog = v.getId();
        showCurrentDialog();

    }

    private boolean is24 = true;

    private Dialog m_Dialog;

    private synchronized void showDialog(Dialog dialog){
        hideDialog();

        m_Dialog = dialog;
        m_Dialog.show();
    }

    private synchronized void hideDialog(){
        if (m_Dialog!=null){
            m_Dialog.dismiss();
            m_Dialog = null;
            m_nCurrentDialog = -1;
        }
    }

    private static final String KEY_CURRENT_DIALOG = "KEY_CURRENT_DIALOG";

    private int m_nCurrentDialog = -1;

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(KEY_CURRENT_DIALOG, m_nCurrentDialog);
        Log.d("devcpp","onSaveInstanceState -> " + m_nCurrentDialog);
    }
}
