package com.kkrasylnykov.l27_dialogsexample;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

/**
 * Created by devcpp on 04.06.2017.
 */

public class ViewInfoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.avtivity_view_info);

        TextView textView = (TextView) findViewById(R.id.TextViewInfo);

        Intent intent = getIntent();
        if (intent.getData()!=null){
            textView.setText(intent.getData().toString());
        }


    }
}
