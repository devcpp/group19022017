package com.kkrasylnykov.l28_externalappexamples;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editText = (EditText) findViewById(R.id.dataEditText);

        findViewById(R.id.browseLinkButton).setOnClickListener(this);
        findViewById(R.id.callPhoneButton).setOnClickListener(this);
        findViewById(R.id.sendMailButton).setOnClickListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("devcpp","onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("devcpp","onResume");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("devcpp","onRestart");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("devcpp","onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("devcpp","onStop");
    }

    @Override
    public void onClick(View v) {
        String strData = editText.getText().toString();
        Intent intent = new Intent();
        switch (v.getId()){
            case R.id.browseLinkButton:
                intent.setAction(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("http://" + strData));
                break;

            case R.id.callPhoneButton:
                //intent.setAction(Intent.ACTION_CALL);
                intent.setAction(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + strData));
                break;

            case R.id.sendMailButton:
                intent.setAction(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_EMAIL  , new String[]{"kos@example.com"});
                intent.putExtra(Intent.EXTRA_SUBJECT, "subject of email");
                intent.putExtra(Intent.EXTRA_TEXT   , strData);
                break;
        }
        startActivity(intent);
    }
}
