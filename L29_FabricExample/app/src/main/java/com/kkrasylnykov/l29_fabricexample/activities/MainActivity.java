package com.kkrasylnykov.l29_fabricexample.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.kkrasylnykov.l29_fabricexample.BuildConfig;
import com.kkrasylnykov.l29_fabricexample.R;
import com.kkrasylnykov.l29_fabricexample.constants.Constants;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.error1).setOnClickListener(this);
        findViewById(R.id.error2).setOnClickListener(this);
        findViewById(R.id.error3).setOnClickListener(this);

        if (BuildConfig.DEBUG){
            //TODO
        }

        Toast.makeText(this, Constants.NAME, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.error1:
                String str = null;
                str.isEmpty();
                break;
            case R.id.error2:
                int n = 1;
                int b = 1/n;
                break;
            case R.id.error3:
                throw new RuntimeException("User click on Error3");
            default:
                throw new NullPointerException();
        }
    }
}
