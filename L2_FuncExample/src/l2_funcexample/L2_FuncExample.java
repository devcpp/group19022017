package l2_funcexample;

import java.util.Scanner;

public class L2_FuncExample {
    
    public static float getFloatAfterInputInts(Scanner s){
        int nDelta1 = s.nextInt();
        int nDelta2 = s.nextInt();
        float fDelta = ((float)nDelta1)/nDelta2;
        return fDelta;
    }
    
    public static int pow2(int n){
        return n*n;
    }
    
    public static float pow2(float n){
        return n*n;
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        
        float fDelta1 = getFloatAfterInputInts(scan);
        //.....
        float fDelta2 = getFloatAfterInputInts(scan);
        //.....
        float fDelta23 = getFloatAfterInputInts(scan);
    }
    
}
