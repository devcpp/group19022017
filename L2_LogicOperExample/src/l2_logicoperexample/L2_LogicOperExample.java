package l2_logicoperexample;

import java.util.Scanner;

public class L2_LogicOperExample {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int a = scan.nextInt();
        int b = scan.nextInt();
        
        float fResult = 0;
        if (a>b && (b!=0|| (b>0 && b<0))){
            fResult = ((float)a)/b;
        } else if (a!=0) {
            fResult = ((float)b)/a;
        } else {
            System.out.println("ERROR!!!!!");
        }
        
        System.out.println("Result = " + fResult);
        
        /*ТАК ДЕЛАТЬ НЕЛЬЗЯ!!!
        boolean bool = a>b;
        if (bool==true){
            
        }*/
    }
    
}
