package l2_powexample;

import java.util.Scanner;

public class L2_PowExample {
    
    public static int pow(int nVal, int nPow){
        int nResult = 0;
        if (nPow==0){
            nResult = 1;
        } else if (nPow>0){
            nResult = nVal * pow(nVal,nPow-1);
        }
        return nResult;
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        
        int x = scan.nextInt();
        int n = scan.nextInt();
        
        int result = pow(x,n);
        
        System.out.println("result = " + result);
    }
    
}
