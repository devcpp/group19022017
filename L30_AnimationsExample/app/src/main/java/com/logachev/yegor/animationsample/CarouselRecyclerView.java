package com.logachev.yegor.animationsample;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Transformation;

/**
 * Created by yegor on 2/16/17.
 */

public class CarouselRecyclerView extends RecyclerView {

    public CarouselRecyclerView(Context context) {
        super(context);
        setStaticTransformationsEnabled(true);
    }

    public CarouselRecyclerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        setStaticTransformationsEnabled(true);
    }

    public CarouselRecyclerView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setStaticTransformationsEnabled(true);
    }

    @Override
    protected boolean getChildStaticTransformation(View child, Transformation t) {
        child.invalidate();
        int childHeight = child.getHeight();
        int childWidth = child.getWidth();
        int parentHeight = getHeight();
        int halfParentHeight = (int) (parentHeight/2 + 0.5f);
        int absChildPosition = Math.abs(child.getTop() + childHeight/2 - halfParentHeight);
        float transformFactor = (float) absChildPosition/halfParentHeight;
        t.setAlpha(0.6f * (1 - transformFactor) + 0.4f);
        float scale = 0.7f * (1 - transformFactor) + 0.3f;
        t.getMatrix().setScale(scale, scale, childWidth/2, childHeight/2);
        return true;
    }
}
