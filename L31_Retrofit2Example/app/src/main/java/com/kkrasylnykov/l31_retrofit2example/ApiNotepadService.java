package com.kkrasylnykov.l31_retrofit2example;


import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ApiNotepadService {

    @Headers("Accept: application/json")
    @GET("/api/users.{format}")
    Call<List<UserInfo>> getAll(@Path("format") String strFormat);

    @Headers("Accept: application/json")
    @POST("/api/users.{format}")
    Call<UserInfo> setUser(@Path("format") String strFormat, @Body UserInfo user);
}
