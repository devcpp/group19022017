package com.kkrasylnykov.l31_retrofit2example;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final TextView textView = (TextView) findViewById(R.id.textInfo);
        findViewById(R.id.addUser).setOnClickListener(this);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://xutpuk.pp.ua")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiNotepadService service = retrofit.create(ApiNotepadService.class);

        service.getAll("json").enqueue(new Callback<List<UserInfo>>() {
            @Override
            public void onResponse(Call<List<UserInfo>> call, Response<List<UserInfo>> response) {
                String strResult = "";
                strResult += "code -> " + response.code() + "\n";
                strResult += "message -> " + response.message() + "\n";
                List<UserInfo> arrData = response.body();
                if (arrData!=null){
                    for (UserInfo item:arrData){
                        strResult += "item -> " + item.toString() + "\n";
                    }
                } else {
                    strResult += "body -> is null\n";
                }
                textView.setText(strResult);
            }

            @Override
            public void onFailure(Call<List<UserInfo>> call, Throwable t) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://xutpuk.pp.ua")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiNotepadService service = retrofit.create(ApiNotepadService.class);
        ArrayList<String> phones = new ArrayList<>();
        phones.add("25062017");
        phones.add("25062018");
        phones.add("25062019");
        UserInfo userInfo = new UserInfo("Kos", "Kras", phones);
        Log.d("devcpp", "userInfo -> " + userInfo.toString());
        service.setUser("json", userInfo).enqueue(new Callback<UserInfo>() {
            @Override
            public void onResponse(Call<UserInfo> call, Response<UserInfo> response) {
                Log.d("devcpp", "onResponse -> " + response.body().toString());
            }

            @Override
            public void onFailure(Call<UserInfo> call, Throwable t) {
                Log.d("devcpp", "onFailure -> ");
            }
        });
    }
}
