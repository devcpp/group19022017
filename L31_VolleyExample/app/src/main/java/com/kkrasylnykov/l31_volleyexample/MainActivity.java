package com.kkrasylnykov.l31_volleyexample;

import android.support.constraint.solver.SolverVariable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView = (TextView) findViewById(R.id.textInfo);
        findViewById(R.id.btnToJSON).setOnClickListener(this);

        RequestQueue queue = Volley.newRequestQueue(this);
        String strURL = "http://xutpuk.pp.ua/api/users.json";

        StringRequest stringRequest = new StringRequest(Request.Method.GET, strURL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //textView.setText("stringRequest -> onResponse" + response);
                Gson gson = new Gson();
                Type listType = new TypeToken<ArrayList<UserInfo>>(){}.getType();
                ArrayList<UserInfo> data = gson.fromJson(response, listType);
                for (UserInfo userInfo:data){
                    textView.setText(textView.getText().toString() + "\n" + userInfo.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                textView.setText("stringRequest -> onErrorResponse" + error.toString());
            }
        });

        queue.add(stringRequest);


    }

    @Override
    public void onClick(View v) {
        ArrayList<SubConvertObj> data = new ArrayList<>();
        for (int i=0; i<10; i++){
            data.add(new SubConvertObj(i, "Name" + i, (i%3==0)));
        }

        TestConvertObj testConvertObj = new TestConvertObj(7, "Type007", data);

        textView.setText(testConvertObj.toString() + "\n\n\n");

        ArrayList<TestConvertObj> datas = new ArrayList<>();
        datas.add(testConvertObj);
        testConvertObj = new TestConvertObj(23, "Type005", null);
        datas.add(testConvertObj);

        Gson gson = new Gson();
        String str = gson.toJson(datas);

        textView.setText(textView.getText().toString() + str);

        /*TestConvertObj o2 = gson.fromJson(str, TestConvertObj.class);


        textView.setText(textView.getText().toString() + "\n\n" + o2);*/
    }
}
