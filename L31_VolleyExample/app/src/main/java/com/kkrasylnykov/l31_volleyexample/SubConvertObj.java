package com.kkrasylnykov.l31_volleyexample;


public class SubConvertObj {

    private long id;
    private String name;
    private boolean active;

    public SubConvertObj(){

    }

    public SubConvertObj(long id, String name, boolean active) {
        this.id = id;
        this.name = name;
        this.active = active;
    }

    @Override
    public String toString() {
        return id + "/" + name + "/" + active ;
    }
}
