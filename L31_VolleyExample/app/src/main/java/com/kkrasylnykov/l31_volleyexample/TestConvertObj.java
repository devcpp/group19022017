package com.kkrasylnykov.l31_volleyexample;

import java.util.ArrayList;

public class TestConvertObj {
    private long id;
    private String type;
    private ArrayList<SubConvertObj> data;

    public TestConvertObj(){

    }

    public TestConvertObj(long id, String type, ArrayList<SubConvertObj> data) {
        this.id = id;
        this.type = type;
        this.data = data;
    }

    @Override
    public String toString() {
        String strResult = "id " + type + ": ";
        if (data!=null){
            for(SubConvertObj item:data){
                strResult += "\n" + item.toString();
            }
        } else {
            strResult +=  "data is null";
        }
        return strResult;
    }
}
