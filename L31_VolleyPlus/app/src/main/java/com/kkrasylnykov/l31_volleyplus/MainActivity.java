package com.kkrasylnykov.l31_volleyplus;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.GsonRequest;
import com.android.volley.toolbox.Volley;


import java.lang.reflect.Type;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final TextView textView = (TextView) findViewById(R.id.textInfo);

        RequestQueue requestQueue = Volley.newRequestQueue(this);

        String strURL = "http://xutpuk.pp.ua/api/users.json";

        GsonRequest<UserInfo[]> request = new GsonRequest<UserInfo[]>(Request.Method.GET,
                strURL, UserInfo[].class, null, null, new Response.Listener<UserInfo[]>() {
            @Override
            public void onResponse(UserInfo[] response) {
                String strResult = "";
                for(UserInfo item:response){
                    strResult += item.toString() + "\n";
                }
                textView.setText(strResult);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                textView.setText("stringRequest -> onErrorResponse" + error.toString());
            }
        });

        requestQueue.add(request);
    }
}
