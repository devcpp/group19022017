package com.kkrasylnykov.l31_volleyplus;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class UserInfo {

    private long m_nId;
    @SerializedName("id")
    private long m_nServerId;
    @SerializedName("name")
    private String m_strName;
    @SerializedName("sname")
    private String m_strSName;

    private ArrayList<String> phones;

    private long m_nBDay;

    public UserInfo(){

    }

    public long getId() {
        return m_nId;
    }

    public void setId(long m_nId) {
        this.m_nId = m_nId;
    }

    public long getServerId() {
        return m_nServerId;
    }

    public void setServerId(long m_nServerId) {
        this.m_nServerId = m_nServerId;
    }

    public String getName() {
        return m_strName;
    }

    public void setName(String m_strName) {
        this.m_strName = m_strName;
    }

    public String getSName() {
        return m_strSName;
    }

    public void setSName(String m_strSName) {
        this.m_strSName = m_strSName;
    }

    public long getBDay() {
        return m_nBDay;
    }

    public void setBDay(long m_nBDay) {
        this.m_nBDay = m_nBDay;
    }

    @Override
    public String toString() {
        String strResult = m_nServerId + " - " + m_strName + " - " + m_strSName + ": ";
        if(phones!=null){
            for (String strPhone:phones){
                strResult += strPhone + ", ";
            }
        } else {
            strResult += "phones==null";
        }
        return strResult;
    }
}
