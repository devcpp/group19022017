package com.kkrasylnykov.l9_elements_example;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView m_textView;
    private EditText m_nameEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        m_textView = (TextView) findViewById(R.id.holloWorldTextViewMainActivity);
        m_textView.setText("Привет мир из кода!!!");

        Button buttonOk = (Button) findViewById(R.id.okButtonMainActivity);
        buttonOk.setOnClickListener(this);

        Button buttonCancel = (Button) findViewById(R.id.cancelButtonMainActivity);
        buttonCancel.setOnClickListener(this);

        m_nameEditText = (EditText) findViewById(R.id.nameEditTextMainActivit);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.okButtonMainActivity:
                String strName = m_nameEditText.getText().toString();
                m_textView.setText("Привет " + strName + "!!!!");
                m_textView.setVisibility(View.VISIBLE);
                break;
            case R.id.cancelButtonMainActivity:
                m_textView.setVisibility(View.INVISIBLE);
                break;
        }
    }
}
